$(document).ready(function() {
    // off canvas
    $('[data-toggle=offcanvas]').click(function() {
        $('.row-offcanvas').toggleClass('active');
    });

    // slider
    $('.bxslider').bxSlider({
        mode: 'fade',
        preloadImages: 'visible',
        auto: true,
        controls: false,
        pager: true,
        infiniteLoop: true,
        pause: 8000
    });

    // bootstrap selectpicker
    $('.bt-select').selectpicker({
      size: 50
    });

    $('select#state_id').change(function(){
        var state = $(this).val();
        
        $.ajax({
            url:'/city/search',
            type:'post',
            data:{state:state}
        }).done(function(data){
            
            $('#city_id').html(data.response);
        });
    });

    $('#frmContato').validate({
        rules:{
            name:{required:true},
            email:{required:true, email:true},
            subject:{required:true},
            message:{required:true}
        },
        messages:{
            name:{required:"Favor preenchercampo obrigatório"},
            email:{required:"Favor preenchercampo obrigatório", email:"Email inválido"},
            subject:{required:"Favor preenchercampo obrigatório"},
            message:{required:"Favor preenchercampo obrigatório"}   
        }
    });

    $('.bt_update_item').change(function(){
        $(this).parent('form').submit();
    });

        //get zipcode 
     $('input[name="cep"]').blur(function(){
      var cep_code = $(this).val();

      if( cep_code.length < 9 ){
        return;
      } else{
        $.get("http://apps.widenet.com.br/busca-cep/api/cep.json", { code: cep_code },
         function(result){
            
            if( result.status!=1 ){
               alert(result.message || "Houve um erro desconhecido");
               return;
            }
           
            $("select#state_id option:contains('"+result.state+"')").prop('selected',true);
            var state = $("select#state_id option:contains('"+result.state+"')").val();
            
            $.ajax({
                url:'/city/search',
                type:'post',
                data:{state:state}
            }).done(function(data){
                
                $('#city_id').html(data.response);
                $('#city_id option:contains('+result.city+')').prop('selected',true);

            });
            
            $('input[name="district"]').val( result.district );
            $('input[name="address"]').val( result.address );
             

            $('input[name="number"]').focus();
            
         });
      }
      
    });

    $('input[name="tel"]').mask('(99) 9999-9999?9');
    $('input[name="cel"]').mask('(99) 9999-9999?9');
    $('input[name="cep"]').mask('99999-999');
    $('input[name="cnpj"]').mask('99.999.999/9999-99');
    $('input[name="rg"]').mask('999.999.999-9');
    $('input[name="cpf"]').mask('999.999.999-99');
    $('input[name="birthday"]').mask('99/99/9999');

    // Cadastro

    $('input[name="estudante"]').click(function(){
        if($(this).val() == 'sim'){
            $('#student_block').fadeIn();
            return;
        }
        $('#student_block').fadeOut();
    });

    $('input[name="document"]').click(function(){
        if($(this).val() == 'cnpj'){
            $('input[name="cpf"]').val('');
            $('input[name="cpf"]').hide();
            $('input[name="cnpj"]').fadeIn();
            
            return;
        }
        $('input[name="cnpj"]').val('');
        $('input[name="cnpj"]').hide();
        $('input[name="cpf"]').fadeIn();
    });

    $('.form-cadastro').validate({
        rules:{
          
             name:{required:true},
            email:{required:true, email:true},
            cpf:{required:true, cpf:true}
        }
    });

});

/*
* Métodos adicionais
*/
jQuery.validator.addMethod("cpf", function(value, element) {
   value = jQuery.trim(value);
    
    value = value.replace('.','');
    value = value.replace('.','');
    cpf = value.replace('-','');
    while(cpf.length < 11) cpf = "0"+ cpf;
    var expReg = /^0+$|^1+$|^2+$|^3+$|^4+$|^5+$|^6+$|^7+$|^8+$|^9+$/;
    var a = [];
    var b = new Number;
    var c = 11;
    for (i=0; i<11; i++){
        a[i] = cpf.charAt(i);
        if (i < 9) b += (a[i] * --c);
    }
    if ((x = b % 11) < 2) { a[9] = 0 } else { a[9] = 11-x }
    b = 0;
    c = 11;
    for (y=0; y<10; y++) b += (a[y] * c--);
    if ((x = b % 11) < 2) { a[10] = 0; } else { a[10] = 11-x; }
    
    var retorno = true;
    if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10]) || cpf.match(expReg)) retorno = false;
    
    return this.optional(element) || retorno;

}, "Informe um CPF válido."); // Mensagem padrão 