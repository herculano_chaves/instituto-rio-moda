$(document).ready(function(){

    $('select#state_id').change(function(){
        var state = $(this).val();
        
        $.ajax({
            url:'/city/search',
            type:'post',
            data:{state:state}
        }).done(function(data){
            
            $('#city_id').html(data.response);
        });
    });

    $('.datepicker').datepicker({
        dateFormat: "dd/mm/yy"
    });

});