@section('content')
<div id="page-contato">

	<div class="row">
		<div class="col-md-12">
			<div class="titulo">
				<div class="txt">
					<h1>Contato</h1>
				</div>
			</div>
		</div>
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-6 right">
			<img src="images/contato/banner.jpg" alt="" class="img-responsive right">
		</div>

		<div class="col-md-6 left">
			<h2>Fale com a gente</h2>
			<p>Caso queira esclarecer dúvidas, obter informações ou enviar sugestões, utilize o formulário abaixo ou entre em contato através dos telefones:</p>
			<p class="fff">&bull; 55 21 2523 - 9975</p>
			<p class="fff">&bull; 55 21 2522 - 2620</p>

			@include('layouts.notifications')
			
			{{ Form::open(['route'=>'contato.post']) }}
				<div class="form-group">
					
					{{ Form::text('name', null, ['placeholder'=>'Digite seu nome', 'class'=>'form-control']) }}
				</div>
				<div class="form-group">
					{{ Form::email('email', null, ['placeholder'=>'Digite seu e-mail', 'class'=>'form-control']) }}
				</div>
				<div class="form-group">
				    {{ Form::text('subject', null, ['placeholder'=>'Digite o assunto', 'class'=>'form-control']) }}
				</div>
				<div class="form-group">
				 
				   {{ Form::textarea('message', null, ['placeholder'=>'Digite sua mensagem', 'class'=>'form-control']) }}
				</div>
				<button type="submit" class="bt-padrao right">Enviar <i class="icone-seta-direita"></i></button>
				<div class="status left"><p class="fff">{{ Session::get('success') or null }}</p></div>				
			{{ Form::close() }}
		</div>		
	</div> <!-- /row -->
	
</div> <!-- /page -->
@stop