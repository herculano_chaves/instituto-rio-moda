@section('content')
<div id="page-imprensa">

	<div class="row">
		<div class="col-md-12">
			<div class="titulo">
				<div class="txt">
					<h1>Imprensa</h1>
				</div>
			</div>
		</div>
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-4">
			<div class="socios imgs-quem-somos">
				<a href="{{ route('imprensaInterna') }}">
					<img src="images/imprensa/img1.jpg" alt="" class="img-responsive">
				</a>
				<h2>
					<a href="{{ route('imprensaInterna') }}">A Moda na luta contra o HIV</a>
				</h2>
				<p>
					<a href="{{ route('imprensaInterna') }}">Associação de lojas de Ipanema se unem para tornar a causa visível.</a>
				</p>
			</div>
		</div>
		<div class="col-md-4">
			<div class="socios imgs-quem-somos">
				<a href="#">
					<img src="images/imprensa/img2.jpg" alt="" class="img-responsive">
				</a>
				<h2>
					<a href="#">POPCORNer</a>
				</h2>		
				<p>
					<a href="#">O Rio Moda está esse mês no evento POPCORNer.</a>
				</p>
			</div>
		</div>
		<div class="col-md-4">
			<div class="socios imgs-quem-somos">
				<a href="#">
					<img src="images/imprensa/img3.jpg" alt="" class="img-responsive">
				</a>
				<h2>
					<a href="#">Blog</a>
				</h2>
				<p>
					<a href="#">Blog do Rio Moda com cara nova.</a>
				</p>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4">
			<div class="socios imgs-quem-somos">
				<a href="#">
					<img src="images/imprensa/img4.jpg" alt="" class="img-responsive">
				</a>
				<h2>
					<a href="#">Rio Moda com mais uma Parceria</a>
				</h2>
				<p>
					<a href="#">Parceria com Instituto Superior de Niterói garante aos alunos desconto.</a>
				</p>
			</div>
		</div>
		<div class="col-md-4">
			<div class="socios imgs-quem-somos">
				<a href="#">
					<img src="images/imprensa/img5.jpg" alt="" class="img-responsive">
				</a>
				<h2>
					<a href="#">Festival Universitário MTV</a>
				</h2>
				<p>
					<a href="#">O Instituto Rio Moda junto com a RedBandana promove o ambiente Fashion & Design Lab.</a>
				</p>
			</div>
		</div>
		<div class="col-md-4">
			<div class="socios imgs-quem-somos">
				<a href="#">
					<img src="images/imprensa/img6.jpg" alt="" class="img-responsive">
				</a>
				<h2>
					<a href="#">Loren Ipsum Título da notícia</a>
				</h2>
				<p>
					<a href="#">Associação de lojas de Ipanema se unem para tornar a causa visível.</a>
				</p>
			</div>
		</div>
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-12">
			<div class="vejamais">
				<span>
					<a href="#" class="bt-next">ver mais</a>
				</span>
				<hr>
			</div>
		</div>
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-6 right">
			<img src="images/imprensa/banner.jpg" width="261" height="190" class="img-responsive right">
		</div>
		<div class="col-md-6 left">
			<h2>O Instituto Rio Moda disponibiliza release sobre suas atividades.</h2>
			<a href="#" class="bt-padrao clear" title="Baixar release">Baixar release <i class="icone-seta-direita"></i></a>
			<h3>Para receber fotos em alta resolução de todos os seus workshops e eventos, entre em contato:</h3>
			<p>&bull; riomoda@institutoriomoda.com.br</p>
			<p>&bull; (21) 2523-9975 e 2227-5094.</p>
			<h3>Acompanhe e divulgue a nossa programação.</h3>
		</div>		
	</div>
	
</div> <!-- /page -->
@stop