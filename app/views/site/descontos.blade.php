@section('content')
<div id="page-descontos">

	<div class="row">
		<div class="col-md-12">
			<div class="titulo">
				<div class="txt">
					<h1>Descontos e conveniados</h1>
				</div>
			</div>
		</div>
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-6">
			<p>Todas as atividades do Instituto Rio Moda podem ser pagas à vista ou parceladas em até 12 vezes em diversos cartões de crédito, com os juros da respectiva bandeira. Aproveite e inscreva-se em mais de uma atividade!</p>
			<h2>Descontos</h2>
			<table class="table">
				<tbody>
					<tr>
						<td>DOIS OU MAIS WORKSHOPS</td>
						<td>5%</td>
					</tr>
					<tr>
						<td>ESTUDANTES COM MATRÍCULA ATIVA</td>
						<td>5%</td>
					</tr>
					<tr>
						<td>ASSOCIADOS ABPModa</td>
						<td>10%</td>
					</tr>
					<tr>
						<td>INSTITUIÇÕES CONVENIADAS</td>
						<td>até 10%</td>
					</tr>
				</tbody>
			</table>
			<p class="fff">OS DESCONTOS SÃO CUMULATIVOS ATÉ O <strong>LIMITE DE 15%</strong></p>
		</div>
		<div class="col-md-6">
			<img src="images/descontos/banner.jpg" alt="" class="img-responsive right">
			<a href="#" class="bt-padrao right clear" title="Compre agora">Compre agora <i class="icone-seta-direita"></i></a>

		</div>
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-12">
			<hr>
			<h2>Conveniados</h2>	
		</div>
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-6">			
			<table class="table">
				<tbody>
					<tr>
						<td class="col-md-2"><img src="images/descontos/conveniados.jpg" height="56" width="56" alt=""></td>
						<td>ABEDESIGN (funcionários de associados)</td>
						<td>5%</td>
					</tr>
					<tr>
						<td><img src="images/descontos/conveniados.jpg" height="56" width="56" alt=""></td>
						<td>ABEST</td>
						<td>5%</td>
					</tr>
					<tr>
						<td><img src="images/descontos/conveniados.jpg" height="56" width="56" alt=""></td>
						<td>ABIT (empresas associadas à ABIT e ao SINDITEXTIL)</td>
						<td>5%</td>
					</tr>
					<tr>
						<td><img src="images/descontos/conveniados.jpg" height="56" width="56" alt=""></td>
						<td>AJORIO (funcionários e associados)</td>
						<td>5%</td>
					</tr>
					<tr>
						<td><img src="images/descontos/conveniados.jpg" height="56" width="56" alt=""></td>
						<td>Alphabeto</td>
						<td>5%</td>
					</tr>
					<tr>
						<td><img src="images/descontos/conveniados.jpg" height="56" width="56" alt=""></td>
						<td>Armário Aberto</td>
						<td>5%</td>
					</tr>
					<tr>
						<td><img src="images/descontos/conveniados.jpg" height="56" width="56" alt=""></td>
						<td>Ateen (Funcionários)</td>
						<td>5%</td>
					</tr>
					<tr>
						<td><img src="images/descontos/conveniados.jpg" height="56" width="56" alt=""></td>
						<td>Blog Modices (Funcionários e Colaboradores)</td>
						<td>5%</td>
					</tr>
					<tr>
						<td><img src="images/descontos/conveniados.jpg" height="56" width="56" alt=""></td>
						<td>Botswana (Funcionários)</td>
						<td>5%</td>
					</tr>
					<tr>
						<td><img src="images/descontos/conveniados.jpg" height="56" width="56" alt=""></td>
						<td>Dress To (funcionários)</td>
						<td>5%</td>
					</tr>
					<tr>
						<td><img src="images/descontos/conveniados.jpg" height="56" width="56" alt=""></td>
						<td>Espaço Fashion (funcionários)</td>
						<td>5%</td>
					</tr>
					<tr>
						<td><img src="images/descontos/conveniados.jpg" height="56" width="56" alt=""></td>
						<td>ESPM (alunos, professores e funcionários)</td>
						<td>5%</td>
					</tr>
					<tr>
						<td><img src="images/descontos/conveniados.jpg" height="56" width="56" alt=""></td>
						<td>Farm (funcionários)</td>
						<td>5%</td>
					</tr>
					<tr>
						<td><img src="images/descontos/conveniados.jpg" height="56" width="56" alt=""></td>
						<td>Fashionistas Por Lei</td>
						<td>5%</td>
					</tr>
					<tr>
						<td><img src="images/descontos/conveniados.jpg" height="56" width="56" alt=""></td>
						<td>Feevale (alunos, professores e funcionários)</td>
						<td>5%</td>
					</tr>
					<tr>
						<td><img src="images/descontos/conveniados.jpg" height="56" width="56" alt=""></td>
						<td>Feranda</td>
						<td>5%</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="col-md-6">
			<table class="table">
				<tbody>
					<tr>
						<td class="col-md-2"><img src="images/descontos/conveniados.jpg" height="56" width="56" alt=""></td>
						<td>Francesca Romana Diana (funcionários e clientes)</td>
						<td>5%</td>
					</tr>
					<tr>
						<td><img src="images/descontos/conveniados.jpg" height="56" width="56" alt=""></td>
						<td>Grupo S2 Holding (Funcionários)</td>
						<td>5%</td>
					</tr>
					<tr>
						<td><img src="images/descontos/conveniados.jpg" height="56" width="56" alt=""></td>
						<td>Grupo Sacada (funcionários)</td>
						<td>5%</td>
					</tr>
					<tr>
						<td><img src="images/descontos/conveniados.jpg" height="56" width="56" alt=""></td>
						<td>La Estampa (funcionários)</td>
						<td>5%</td>
					</tr>
					<tr>
						<td><img src="images/descontos/conveniados.jpg" height="56" width="56" alt=""></td>
						<td>Maria Filó (funcionários)</td>
						<td>5%</td>
					</tr>
					<tr>
						<td><img src="images/descontos/conveniados.jpg" height="56" width="56" alt=""></td>
						<td>Marie/Rudge (funcionários e colaboradores)</td>
						<td>5%</td>
					</tr>
					<tr>
						<td><img src="images/descontos/conveniados.jpg" height="56" width="56" alt=""></td>
						<td>Osklen (funcionários)</td>
						<td>5%</td>
					</tr>
					<tr>
						<td><img src="images/descontos/conveniados.jpg" height="56" width="56" alt=""></td>
						<td>PUC-Rio (alunos, professores e funcionários)</td>
						<td>5%</td>
					</tr>
					<tr>
						<td><img src="images/descontos/conveniados.jpg" height="56" width="56" alt=""></td>
						<td>Sala de Estar (Funcionários e Clientes)</td>
						<td>5%</td>
					</tr>
					<tr>
						<td><img src="images/descontos/conveniados.jpg" height="56" width="56" alt=""></td>
						<td>Shop 126 (funcionários)</td>
						<td>5%</td>
					</tr>
					<tr>
						<td><img src="images/descontos/conveniados.jpg" height="56" width="56" alt=""></td>
						<td>Toulon (Funcionários)</td>
						<td>5%</td>
					</tr>
					<tr>
						<td><img src="images/descontos/conveniados.jpg" height="56" width="56" alt=""></td>
						<td>Unisinos (alunos)</td>
						<td>5%</td>
					</tr>
					<tr>
						<td><img src="images/descontos/conveniados.jpg" height="56" width="56" alt=""></td>
						<td>UVA (alunos, professores e funcionários)</td>
						<td>5%</td>
					</tr>
					<tr>
						<td><img src="images/descontos/conveniados.jpg" height="56" width="56" alt=""></td>
						<td>Via Mia (funcionários)</td>
						<td>5%</td>
					</tr>
					<tr>
						<td><img src="images/descontos/conveniados.jpg" height="56" width="56" alt=""></td>
						<td>Wöllner (funcionários)</td>
						<td>5%</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div> <!-- /row -->

</div> <!-- /page -->
@stop