@section('content')
<div id="page-home">
	
	<div class="row">
		<div class="col-md-12">

			{{ $slider_home_featured }}

		</div>
	</div> <!-- /row -->

	<div class="row">
		@foreach($products_featured_2 as $p)
		<div class="col-lg-6 col-sm-6">
			<div class="grid">
				
				<figure class="effect-bubba secundario">
					<img src="{{ asset('uploads/eventos/medium/'.$p->src) }}" width="100%" height="279" class="img-responsive" />
					<figcaption>
						<div class="box-txt">
							<h3>{{ Helper::Dia($p->date_ini, true) }} e {{ Helper::Dia($p->date_fin, true) }} de {{ Helper::mesExtenso($p->date_fin) }} &diams; {{ $p->category->name or null }}</h3>
							<h2>{{ $p->title->name or null }} @if(!empty($p->title->name) and !empty($p->name)) - @endif{{ $p->name or null }}</h2>
							<p>facilitado por <strong>{{ $p->facilitator->name or null }}</strong></p>							
							<a href="{{ route('events.show',$p->slug) }}" title="Inscreva-se no {{ $p->category->name }}"></a>
						</div>
					</figcaption>	
				</figure>
			</div>
		</div>
		@endforeach
	</div> <!-- /row -->

	<div class="row">
		<div class="espaco-banners">
			@foreach($products_featured_3 as $p)
			<div class="col-lg-4 col-sm-6 espaco-banners-responsivo">
				<div class="grid">
				<figure class="effect-bubba terciario">
					<img src="{{ asset('uploads/eventos/'.$p->src) }}" width="100%" height="161" class="img-responsive" />
					<figcaption>
						<div class="box-txt">
							<h3>{{ Helper::Dia($p->date_ini, true) }} e {{ Helper::Dia($p->date_fin, true) }} de {{ Helper::mesExtenso($p->date_fin) }} &diams; {{ $p->category->name or null }}</h3>
							<h2>{{ $p->title->name or null }} @if(!empty($p->title->name) and !empty($p->name)) - @endif{{ $p->name or null }}</h2>
													
							<a href="{{ route('events.show',$p->slug) }}" title="Inscreva-se no {{ $p->category->name }}"></a>
						</div>
					</figcaption>	
				</figure>	
				</div>
			</div>
			@endforeach
			<div class="col-lg-4 col-sm-6 espaco-banners-responsivo">
				@if(isset($banner_1->id))
				<a href="{{ $banner_1->link }}" title="{{ $banner_1->name }}"><img src="{{ asset('uploads/banner/'.$banner_1->src) }}" width="100%" class="img-responsive" /></a>
				@endif
			</div>
			<div class="col-lg-4 col-sm-6 espaco-banners-responsivo">
				@if(isset($banner_2->id))
				<a href="{{ $banner_2->link }}" title="{{ $banner_2->name }}"><img src="{{ asset('uploads/banner/'.$banner_2->src) }}" width="100%" class="img-responsive" /></a>
				@endif
			</div>
		</div>
	</div> <!-- /row -->

	<div class="row">
		<div class="espaco-banners">
			<div class="col-md-12">


				@if(isset($banner_main->id))
				<a href="{{ $banner_main->link }}" title="{{ $banner_main->name }}"><img src="{{ asset('uploads/banners/'.$banner_main->src) }}" width="100%" class="img-responsive" /></a>
				@endif
			</div>
		</div>
	</div> <!-- /row -->

</div> <!-- /page -->
@stop