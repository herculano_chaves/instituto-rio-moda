@section('content')
<div id="page-sacola">

	<div class="row">
		<div class="col-md-12">
			<div class="titulo">
				<div class="txt">
					<h1>Minha sacola</h1>
				</div>
			</div>
		</div>
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-12">
			<h2>Workshops</h2>
			<table class="table">
				<tbody>
					<tr>
						<td>SÃO PAULO - Novas ferramentas do Marketing de Moda - André Carvalhal</td>
						<td>
							<select class="bt-select">
								<option>X1</option>
								<option>X2</option>
								<option>X3</option>
								<option>X4</option>
								<option>X5</option>
							</select>
						</td>
						<td>R$ 1.238,00</td>
					</tr>
					<tr>
						<td>RIO - Formação de Preço em Moda - Roberto Assef</td>
						<td>
							<select class="bt-select">
								<option>X1</option>
								<option>X2</option>
								<option>X3</option>
								<option>X4</option>
								<option>X5</option>
							</select>
						</td>
						<td>R$ 899,00</td>
					</tr>
					<tr>
						<td>RIO - Estratégias Multicanais em Moda - Claudio Santos Junior</td>
						<td>
							<select class="bt-select">
								<option>X1</option>
								<option>X2</option>
								<option>X3</option>
								<option>X4</option>
								<option>X5</option>
							</select>
						</td>
						<td>R$ 1.112</td>
					</tr>
					<tr>
						<td>Valor total</td>
						<td></td>
						<td>R$ 3.249,00</td>
					</tr>
					<tr>
						<td>Desconto aplicado</td>
						<td></td>
						<td>10%</td>
					</tr>
					<tr class="valor-final">
						<td>Valor final</td>
						<td></td>
						<td>R$ 2.924,00</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-8">
			<h2>Formas de pagamento</h2>
			<p>Selecione a forma de pagamento.</p>
			
			<form role="form">
				<div class="box-borda">
				
					<div class="form-inline form-group">
				    	<label class="radio-inline">
							<input type="radio" name="pagamento" id="pagseguro" value="pagseguro"> <img src="images/logo-pagseguro.png" alt="">
						</label>
					</div>
					<p>Escolhemos a empresa Pag Seguro/UOL para efetuar nossas transações online com o objetivo de oferecer a você mais conforto, segurança e opções de pagamento.</p>
					<img src="images/opcoes-pagamentos.jpg" alt="" class="img-responsive">
				</div> <!-- /box-borda -->	
		
				<div class="box-borda">
					<div class="form-inline form-group">
				    	<label class="radio-inline">
							<input type="radio" name="pagamento" id="deposito" value="deposito"> Depósito em conta
						</label>
					</div>
					<p>Banco: Nome do Banco</p>
					<p>
						Agência: 9999-9 <br/>
						Conta: 999999-9
					</p>
				
				</div> <!-- /box-borda -->	
			</form>
		</div>
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-12">
			<hr>
			<button type="submit" class="bt-padrao right">Finalizar compra <i class="icone-seta-direita"></i></button>
			<button type="submit" class="bt-padrao right">Continuar comprando <i class="icone-seta-direita"></i></button>			
		</div>
	</div> <!-- /row -->

</div> <!-- /page -->
@stop