@section('content')
<div id="page-pedidos">

	<div class="row">
		<div class="col-md-12">
			<div class="titulo">
				<div class="txt">
					<h1>Meus pedidos</h1>
				</div>
			</div>
		</div>
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-12">
			<h2>Eventos agendados</h2>
			<div class="agendados">
				<div class="info col-md-8">
					<p><span>SÃO PAULO - Novas ferramentas do Marketing de Moda - André Carvalhal</span></p>
					<p>Data: 15 de setembro de 2015</p>
					<p>Hora: das 8h30 às 18h00</p>
					<p><span>Valor Total: R$ 2.924,00</span></p>
				</div>	
				<div class="col-md-4">
					<p><i class="fa fa-exclamation-circle"></i> <span>Aguardando confirmação de pagamento</span></p>
				</div>
			</div> <!-- /evento -->

			<div class="agendados">
				<div class="info col-md-8">
					<p><span>SÃO PAULO - Novas ferramentas do Marketing de Moda - André Carvalhal</span></p>
					<p>Data: 15 de setembro de 2015</p>
					<p>Hora: das 8h30 às 18h00</p>
					<p><span>Valor Total: R$ 2.924,00</span></p>
				</div>	
				<div class="col-md-4">
					<p><i class="fa fa-check-circle"></i> <span>Pagamento confirmado</span></p>
				</div>
			</div> <!-- /evento -->		
		</div>
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-12">
			<h2>Histórico de eventos</h2>
			<div class="historico">
				<div class="col-md-12">
					<p>SÃO PAULO - Novas ferramentas do Marketing de Moda - André Carvalhal</p>
					<p>Data: 15 de janeiro de 2015</p>
					<p>Hora: das 8h30 às 18h00</p>
					<p>Valor Total: R$ 2.924,00</p>
				</div>
			</div>
			<div class="historico">
				<div class="col-md-12">
					<p>SÃO PAULO - Novas ferramentas do Marketing de Moda - André Carvalhal</p>
					<p>Data: 15 de janeiro de 2015</p>
					<p>Hora: das 8h30 às 18h00</p>
					<p>Valor Total: R$ 2.924,00</p>
				</div>
			</div>							
		</div>
	</div> <!-- /row -->

</div> <!-- /page -->
@stop