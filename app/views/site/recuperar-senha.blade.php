@section('content')
<div id="page-recuperar-senha">

	<div class="row">
		<div class="col-md-12">
			<div class="titulo">
				<div class="txt">
					<h1>Recuperar senha</h1>
				</div>
			</div>
		</div>
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-6">
			<h2>Esqueceu sua senha?</h2>
			<p>Sem problemas. Coloque seu e-mail no campo abaixo, e enviaremos uma nova senha para o seu e-mail.</p>
			<form role="form" class="form-cadastro">
				<div class="form-group">
					<input type="email" class="form-control" id="email" placeholder="Digite seu e-mail">
				</div>
				<button type="submit" class="bt-padrao right">Enviar <i class="icone-seta-direita"></i></button>
			</form class="clear"> <!-- /form -->
		</div>		
	</div> <!-- /row -->
	
</div> <!-- /page -->
@stop