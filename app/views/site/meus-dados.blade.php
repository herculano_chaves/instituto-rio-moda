@section('content')
<div id="page-dados">

	<div class="row">
		<div class="col-md-12">
			<div class="titulo">
				<div class="txt">
					<h1>Meus dados</h1>
				</div>
			</div>
		</div>
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-6">
			<h2>Alterar senha</h2>
			<p>Enviaremos um e-mail com as informações e um link para o seu e-mail cadastrado.</p>
			<form role="form" class="form-dados">
				<a href="minha-sacola.html" class="bt-padrao">Entrar <i class="icone-seta-direita"></i></a>
				<!-- <button type="submit" class="bt-padrao right">Entrar <i class="icone-seta-direita"></i></button> -->
			</form class="clear"> <!-- /form -->
		</div>		
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-6">
			<h2>Campos obrigatórios</h2>
			<p>Cadastre-se para inscrever-se em nossas atividades. É rápido e fácil.</p>
			<h3>Campos obrigatórios</h3>
			<form role="form" class="form-dados">				
				<div class="form-group">
					<input type="text" class="form-control" id="nome" placeholder="Nome completo">
				</div>
				<div class="form-group">
					<input type="email" class="form-control" id="email" placeholder="E-mail">
				</div>
				<div class="form-inline form-group">
			    	<label class="radio-inline">
						<input type="radio" name="cpf-cnpj" id="cpf" value="cpf"> CPF
					</label>
					<label class="radio-inline">
						<input type="radio" name="cpf-cnpj" id="cnpj" value="cnpj"> CNPJ
					</label>
					<input type="email" class="form-control" id="email" placeholder="Digite seu CPF">	
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="nome-instituicao" placeholder="Nome Instituição">
				</div>
				<div class="form-group">
					<select class="bt-select">
						<option>Instituição de Moda</option>
						<option>2</option>
						<option>3</option>
						<option>4</option>
						<option>5</option>
					</select>
				</div>
				<div class="form-group">
					<select class="bt-select">
						<option>Superior Completo</option>
						<option>2</option>
						<option>3</option>
						<option>4</option>
						<option>5</option>
					</select>
				</div>
				<div class="form-group">
					<select class="bt-select">
						<option>Como ficou sabendo desta iniciativa?</option>
						<option>Newsletter</option>
						<option>3</option>
						<option>4</option>
						<option>5</option>
					</select>
				</div>
				<div class="form-group">
					<p>É estudante?</p>
					<label class="radio-inline">
						<input type="radio" name="estudante" id="estudante-sim" value="sim"> Sim
					</label>
					<label class="radio-inline">
						<input type="radio" name="estudante" id="estudante-nao" value="não"> Não
					</label>
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="curso" placeholder="Curso">
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="instituicao-de-ensino" placeholder="Instituição de ensino">
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="ano-da-graduacao" placeholder="Ano da graduação">
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="matricula" placeholder="Matrícula">
				</div>

				<h3>Campos complementares</h3>
				<div class="form-group">
					<input type="text" class="form-control" id="telefone" placeholder="Telefone para contato">
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="data-de-nascimento" placeholder="Data de nascimento">
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="identidade" placeholder="Identidade">
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="orgao-expedidor" placeholder="Órgão expedidor">
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="site-blog" placeholder="Site / Blog">
				</div>
				<div class="form-group">
					<select class="bt-select">
						<option>Escolaridade</option>
						<option>2</option>
						<option>3</option>
						<option>4</option>
						<option>5</option>
					</select>
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="cep" placeholder="CEP">
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="endereco" placeholder="Endereço">
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="numero" placeholder="Número">
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="complemento" placeholder="Complemento">
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="bairro" placeholder="Bairro">
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="cidade" placeholder="Cidade">
				</div>
				<div class="form-group">
					<select class="bt-select">
						<option>UF</option>
						<option>2</option>
						<option>3</option>
						<option>4</option>
						<option>5</option>
					</select>
				</div>

				<button type="submit" class="bt-padrao right">Atualizar <i class="icone-seta-direita"></i></button>
			</form> <!-- /form -->
		</div>
	</div> <!-- /row -->
	
</div> <!-- /page -->
@stop