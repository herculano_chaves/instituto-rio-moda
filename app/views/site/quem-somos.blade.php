@section('content')
<div id="page-quem-somos">

	<div class="row">
		<div class="col-md-12">
			<div class="titulo">
				<div class="txt">
					<h1>Quem somos</h1>
				</div>
			</div>
		</div>
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-4">
			<h2>Missão</h2>
			<p>O Instituto Rio Moda existe para abrir espaço a talentos da moda e para contribuir no desenvolvimento de competências profissionais, visando à evolução da indústria.</p>
		</div>
		<div class="col-md-4">
			<h2>Visão</h2>
			<p>Em 2018, queremos ser reconhecidos como um Instituto que resgatou, movimentou e revolucionou a indústria brasileira de moda, a partir dos melhores fundamentos do "aprender fazendo".</p>
		</div>
		<div class="col-md-4">
			<h2>Valores</h2>
			<ul class="valores">
				<li>Sustentabilidade</li>
				<li>Profissionalismo</li>		
				<li>Diversidade</li>
				<li>Coragem</li>
				<li>Paixão</li>
				<li>Ética</li>
			</ul>
		</div>
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-12">
			<hr>
		</div>
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-12">
			<h2>Sócios</h2>
			<h3>Com o que nos parecemos?</h3>
			<p>Trata-se uma sociedade limitada que possui dois sócios fundadores, todos com experiência em moda ou em educação (ou em ambos), que também controlam uma associação sem fins lucrativos.</p>
		</div>
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-6">
			<div class="socios imgs-quem-somos">
				<img src="images/quem-somos/socio1.jpg" alt="" class="img-responsive">
				<h2>Roberto Meireles</h2>
				<p>Mestre em Educação pela PUC-Rio, Pós-graduado em Administração pela Coopead UFRJ, Engenheiro pela UFRJ. Desde 1989, reúne experiências como docente e gestor de ensino superior, com forte atuação na consultoria em Educação e no campo do e-learning.</p>
			</div>
		</div>
		<div class="col-md-6">
			<div class="socios imgs-quem-somos">
				<img src="images/quem-somos/socio2.jpg" alt="" class="img-responsive">
				<h2>Alessandra Marins</h2>
				<p>Pós-graduada em Cultura em Moda pela Universidade Anhembi Morumbi e em Gestão Empresarial pela Universidade Candido Mendes, Jornalista pela FACHA. Desde 1993, reúne experiência como estilsta e compradora, desde 2000 como consultora de moda e desde 2004, como docente em cursos de pós-graduação.</p>
			</div>
		</div>
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-12">
			<hr>
		</div>
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-12">
			<h2>Quem está conosco</h2>
			<p>Estudantes de moda, marketing, comunicação, administração e design; profissionais de múltiplos segmentos da moda, empresários, novos empreendedores; e interessados no tema.</p>
		</div>

		<div class="col-md-12">
			<ul class="facilitadores">
				<li>
					<img src="images/quem-somos/facilitador1.jpg" alt="">
					<a href="#">Nome facilitador</a>
				</li>
				<li>
					<img src="images/quem-somos/facilitador2.jpg" alt="">
					<a href="#">Nome facilitador</a>
				</li>
				<li>
					<img src="images/quem-somos/facilitador3.jpg" alt="">
					<a href="#">Nome facilitador</a>
				</li>
				<li>
					<img src="images/quem-somos/facilitador4.jpg" alt="">
					<a href="#">Nome facilitador</a>
				</li>
				<li>
					<img src="images/quem-somos/facilitador5.jpg" alt="">
					<a href="#">Nome facilitador</a>
				</li>
				<li>
					<img src="images/quem-somos/facilitador6.jpg" alt="">
					<a href="#">Nome facilitador</a>
				</li>
				<li>
					<img src="images/quem-somos/facilitador7.jpg" alt="">
					<a href="#">Nome facilitador</a>
				</li>
				<li>
					<img src="images/quem-somos/facilitador8.jpg" alt="">
					<a href="#">Nome facilitador</a>
				</li>
				<li>
					<img src="images/quem-somos/facilitador9.jpg" alt="">
					<a href="#">Nome facilitador</a>
				</li>
				<li>
					<img src="images/quem-somos/facilitador10.jpg" alt="">
					<a href="#">Nome facilitador</a>
				</li>
				<li>
					<img src="images/quem-somos/facilitador11.jpg" alt="">
					<a href="#">Nome facilitador</a>
				</li>
				<li>
					<img src="images/quem-somos/facilitador12.jpg" alt="">
					<a href="#">Nome facilitador</a>
				</li>
			</ul>
		</div>
	</div> <!-- /row -->
	
</div> <!-- /page -->
@stop