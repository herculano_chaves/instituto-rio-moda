@section('content')
<div id="page-imprensa">

	<div class="row">
		<div class="col-md-12">
			<div class="titulo">
				<div class="txt">
					<h1>Imprensa</h1>
				</div>
			</div>
		</div>
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-6 right">
			<img src="images/imprensa/interna-banner.jpg" alt="" class="img-responsive right">
		</div>

		<div class="col-md-6 left">
			<h2>Festival Universitário MTV</h2>
			<p>Nos dias 19, 20 e 21 de novembro, o evento ofereceu o espaço Fashion & Design Lab e o conceituado Mercado Mistureba, com aproximadamente 30 estandes. Embalado pela trilha sonora do The Walkmen, Móveis Coloniais de Acaju, Moptop, Rockinho e por outras promessas da cena independente brasileira, o público pode garimpar no Festival Universitário MTV as peças únicas de estilistas e criadores que são promessas na moda alternativa carioca.</p>
			<p>Além da participação das dez bandas selecionadas por votação popular para subir ao palco do FU MTV, o público também pode colocar a mão na massa e liberar a criatividade para atacar de estilista e desenvolver suas camisetas personalizadas. No Fashion & Design Lab, desenvolvido pelo Instituto Rio Moda, foram vendidas camisetas com a logo do festival para serem customizadas com patches, miçangas, cortes e modelagens diferentes. A demanda por peças exclusivas são uma aposta da moda contemporânea. "Em tempos de mercados hiper segmentados, a moda alternativa vem atender a necessidade de personalizar o vestuário com peças únicas", explica Roberto Meireles, do Instituto Rio Moda.</p>
		</div>		
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-12">
			<div class="barra-opcoes">
				<a href="{{ route('imprensa') }}" class="left"><i class="glyphicon glyphicon-th"></i> Voltar para listagem</a>

				<a href="#" class="right"><i class="glyphicon glyphicon-print"></i> Imprimir</a>
				<a href="#" class="right"><i class="glyphicon glyphicon-share"></i> Enviar para um amigo</a>
			</div> <!-- /barra -->
		</div>	
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-6 right">
			<img src="images/imprensa/banner.jpg" width="261" height="190" class="img-responsive right">
		</div>
		<div class="col-md-6 left">
			<h2>O Instituto Rio Moda disponibiliza release sobre suas atividades.</h2>
			<a href="#" class="bt-padrao clear" title="Baixar release">Baixar release <i class="icone-seta-direita"></i></a>
			<h3>Para receber fotos em alta resolução de todos os seus workshops e eventos, entre em contato:</h3>
			<p>&bull; riomoda@institutoriomoda.com.br</p>
			<p>&bull; (21) 2523-9975 e 2227-5094.</p>
			<h3>Acompanhe e divulgue a nossa programação.</h3>
		</div>		
	</div>
	
</div> <!-- /page -->
@stop