@section('content')
<div id="page-atividades">

	<div class="row">
		<div class="col-md-12">
			<div class="titulo">
				<div class="txt">
					<h1>Portfólio de atividades</h1>
				</div>			
			</div>
		</div>
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-12">
			<h2>Categorias</h2>
		</div>
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-12">
			<ul class="nav nav-tabs">
			    <li class="active"><a data-toggle="tab" href="#workshops">Workshops</a></li>
			    <li><a data-toggle="tab" href="#talkshows">Talkshows</a></li>
			    <li><a data-toggle="tab" href="#programas_especiais">Programas especiais</a></li>
				<li class="dropdown">
			    	<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">
			    		Todos os temas<span class="caret"></span>
			    	</a>
				    <ul class="dropdown-menu" role="menu">
				    	<li><a data-toggle="tab" href="#todos_os_meses">Todos os temas</a></li>
				    </ul>				    
				</li>
				<li class="dropdown">
			    	<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">
			    		Mês<span class="caret"></span>
			    	</a>
				    <ul class="dropdown-menu" role="menu">
				    	<li><a data-toggle="tab" href="#todos_os_meses">Todos os meses (17)</a></li>
				    	<li><a data-toggle="tab" href="#">Abril 2015 (2)</a></li>
				    	<li><a data-toggle="tab" href="#">Maio 2015 (2)</a></li>
				    	<li><a data-toggle="tab" href="#">Junho 2015 (2)</a></li>
				    	<li><a data-toggle="tab" href="#">Setembro 2015 (2)</a></li>
				    </ul>				    
				</li>
		  	</ul>
		</div>
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-12">
			<div class="tab-content">

			    <div id="workshops" class="tab-pane fade in active">
			    	<div class="col-md-6 right">
						<img src="images/frentes-de-atuacao/banner.jpg" alt="" class="img-responsive right">
						<div class="informacoes">
							<select class="bt-select">
								<option selected>Informações específicas</option>
								<option>X2</option>
								<option>X3</option>
								<option>X4</option>
								<option>X5</option>
							</select>
							<a href=""><i class="icone-pdf"></i></a>
						</div>
						<a href="#" class="bt-padrao right clear" title="Inscreva-se">Inscreva-se <i class="icone-seta-direita"></i></a>
					</div>
					<div class="col-md-6 left">
						<h3>RIO - Fashion Tracking</h3>
						<p><span>Nome da atividade:</span> RIO - Fashion Tracking</p>
						<p><span>Duração:</span> 36 horas</p>
						<p><span>Data de Início:</span> 27/04/2015</p>
						<p><span>Data de Término:</span> 30/04/2015</p>
						<p><span>Horário:</span> 9h às 18h</p>
						<p><span>Conteúdo:</span> A proposta é possibilitar aos participantes construir uma visão abrangente das marcas que influenciam e inspiram o estilo de vida carioca, dando-lhes a oportunidade de desenvolver uma compreensão mais profunda da moda do Rio de Janeiro. O programa contempla palestras e visitas a uma seleção especial de lojas e fábricas, bem como encontros com representantes de marcas muito relevantes no cenário brasileiro de moda.</p>
						<p><span>Local:</span> Ruas de Ipanema e Leblon, - - - - -, Rio de Janeiro - RJ</p>
						<p><span>Preço:</span> R$ 2800,00</p>
					</div>
					

					<div class="col-md-12 clear">
						<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingOne">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="collapsed">
											Confira os descontos
										</a>
									</h4>
								</div>
								<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
									<div class="panel-body">
										<h2>Preços por programas especiais e descontos</h2>
										<p>Todas as atividades do Rio Moda poderão ser pagas à vista ou parceladas em até 12 vezes no cartão. Aproveite e junte-se a nós em mais de uma atividade!</p>
										<table class="table">
											<thead>
												<tr>
													<th>Tipo de compra</th>
													<th>Valor (R$)</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>1 único workshop</td>
													<td>R$ 2800,00</td>
												</tr>
												<tr>
													<td>2 ou mais wkp tem-se desconto de 5%</td>
													<td></td>
												</tr>
												<tr>
													<td>Associados ABPModa têm desconto de 5%</td>
													<td></td>
												</tr>
												<tr>
													<td>Estudantes têm desconto de 5%</td>
													<td></td>
												</tr>
												<tr>
													<td>Instituições conveniadas têm desconto de até 5%</td>
													<td></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div> <!-- /panel -->

							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingTwo">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" class="collapsed">
											Eventos relacionados
										</a>
									</h4>
								</div>
								<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
									<div class="panel-body">
										<p><span>RIO - </span>Direção de Arte para Campanhas de Moda - Eduardo Varela</p>
										<p><span>RIO - </span>Branding nas Redes Sociais - Igor Fidalgo</p>
										<p><span>RIO - </span>Fashion Film - Douglas Clayton</p>
										<p><span>RIO - </span>Gestão de Produto e Logística em Moda - Daniela Valadão</p>
										<p><span>RIO - </span>Visual Merchandising - Natália Coutinho</p>
									</div>
								</div>
							</div> <!-- /panel -->
						</div> <!-- /panel-group -->

						<div class="col-md-12">
							<h2>Facilitado por:</h2>
							<div class="col-md-2">
								<img src="images/frentes-de-atuacao/avatar-facilitador.jpg" alt="">
							</div>	
							<div class="col-md-10">
								<h3>Nome do facilitador</h3>
								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. ex ea commodo consequat.</p>
								<p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
								<p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam. nitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>
							</div>
						</div>

						<div class="col-md-12">
							<div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">

								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingTree">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion2" href="#collapseTree" aria-expanded="true" aria-controls="collapseTree" class="collapsed">
												Outros eventos deste facilitador
											</a>
										</h4>
									</div>
									<div id="collapseTree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTree">
										<div class="panel-body">
											<p><span>RIO - </span>Direção de Arte para Campanhas de Moda - Eduardo Varela</p>
											<p><span>RIO - </span>Branding nas Redes Sociais - Igor Fidalgo</p>
											<p><span>RIO - </span>Fashion Film - Douglas Clayton</p>
											<p><span>RIO - </span>Gestão de Produto e Logística em Moda - Daniela Valadão</p>
											<p><span>RIO - </span>Visual Merchandising - Natália Coutinho</p>
										</div>
									</div>
								</div> <!-- /panel -->

							</div> <!-- /panel-group -->
						</div> <!-- /col -->

					</div> <!-- /col -->
			    </div> <!-- /workshops -->

			    <div id="talkshows" class="tab-pane fade">
			    	<div class="col-md-12">
						<h3>Menu 1</h3>
						<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
					</div>
			    </div> <!-- /talkshows -->

			    <div id="programas_especiais" class="tab-pane fade">
			    	<div class="col-md-12">
						<h3>Menu 2</h3>
						<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
					</div>
			    </div> <!-- /programas_especiais -->

			    <div id="todos_os_meses" class="tab-pane fade">
			    	<div class="col-md-12">
						<p>
							<a href="#"><span>12/05/2015 - </span>Direção de Arte para Campanhas de Moda - Eduardo Varela</a>
						</p>

						<p>
							<a href="#"><span>17/05/2015 - </span>Direção de Arte para Campanhas de Moda - Eduardo Varela</a>
						</p>

						<p>
							<a href="#"><span>03/06/2015 - </span>Direção de Arte para Campanhas de Moda - Eduardo Varela</a>
						</p>

						<p>
				      		<a href="#"><span>03/06/2015 - </span>Direção de Arte para Campanhas de Moda - Eduardo Varela</a>
						</p>

						<p>
							<a href="#"><span>03/06/2015 - </span>Direção de Arte para Campanhas de Moda - Eduardo Varela</a>
						</p>

						<p>
							<a href="#"><span>03/06/2015 - </span>Direção de Arte para Campanhas de Moda - Eduardo Varela</a>
						</p>

						<p>
							<a href="#"><span>03/06/2015 - </span>Direção de Arte para Campanhas de Moda - Eduardo Varela</a>
						</p>

						<p>
							<a href="#"><span>03/06/2015 - </span>Direção de Arte para Campanhas de Moda - Eduardo Varela</a>
						</p>

						<p>
							<a href="#"><span>03/06/2015 - </span>Direção de Arte para Campanhas de Moda - Eduardo Varela</a>
						</p>

						<p>
							<a href="#"><span>03/06/2015 - </span>Direção de Arte para Campanhas de Moda - Eduardo Varela</a>
						</p>

						<p>
							<a href="#"><span>03/06/2015 - </span>Direção de Arte para Campanhas de Moda - Eduardo Varela</a>
						</p>

						<p>
							<a href="#"><span>03/06/2015 - </span>Direção de Arte para Campanhas de Moda - Eduardo Varela</a>
						</p>

						<p>
							<a href="#"><span>03/06/2015 - </span>Direção de Arte para Campanhas de Moda - Eduardo Varela</a>
						</p>

						<p>
							<a href="#"><span>03/06/2015 - </span>Direção de Arte para Campanhas de Moda - Eduardo Varela</a>
						</p>

						<p>
							<a href="#"><span>03/06/2015 - </span>Direção de Arte para Campanhas de Moda - Eduardo Varela</a>
						</p>
					</div>
			    </div> <!-- /todos_os_meses -->

			    <div class="col-md-12">
				</div> <!-- /col -->

			</div> <!-- /tab-content -->
		</div>
	</div> <!-- /row -->

	<div class="row">
		
	</div> <!-- /row -->

</div> <!-- /page -->
@stop