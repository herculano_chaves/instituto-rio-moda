<!DOCTYPE>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Instituto Rio Moda</title>
	{{ HTML::style('css/bootstrap.css') }}
	{{ HTML::style('css/set1.css') }}
	{{ HTML::style('lib/jquery.bxslider.css') }}
	{{ HTML::style('css/style.css') }}
	
	<link href="http://netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">	
</head>
<body>
	<div class="row-offcanvas row-offcanvas-left">
		@include('layouts.partials.sidebar')

		<div id="main">
			<div class="col-md-12">

				@include('layouts.partials.topo-main')				

				@yield('content')

				@include('layouts.partials.footer')

			</div> <!-- /col-md-12 -->
		</div> <!-- /main -->

	</div> <!--/row-offcanvas -->

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	

	{{ HTML::script('js/bootstrap.min.js') }}
	{{ HTML::script('js/bootstrap-select.min.js') }}
	{{ HTML::script('js/jquery.easing.1.3.js') }}
	{{ HTML::script('js/holder.js') }}
	{{ HTML::script('lib/jquery.bxslider.js') }}
	{{ HTML::script('js/jquery.validate.js') }}
	{{ HTML::script('js/jquery.mask.js') }}
	{{ HTML::script('js/main.js') }}

</body>
</html>