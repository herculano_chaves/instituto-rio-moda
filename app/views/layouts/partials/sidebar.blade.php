<div id="sidebar" class="sidebar-offcanvas">
	<div class="col-md-12">
		<a href="{{ route('home') }}" class="hidden-xs">
			<img src="{{asset('images/logo-irm.png')}}" class="img-responsive logo"/>
		</a>
		<ul class="nav menu">
			<li><a href="{{ route('home') }}">Home</a></li>
			<li><a href="{{ route('quemSomos') }}">Quem somos</a></li>
			<li><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">Produtos <i class="icone-seta-direita right"></i></a>
				<ul class="dropdown-menu">
					@foreach($taxonomies as $t)
			      		<li><a href="{{ route('events.index', $t->slug) }}">{{ $t->name }}</a></li>
			      	@endforeach
			    </ul>
			</li>
			<li><a href="{{ route('descontos') }}">Descontos e conveniados</a></li>
			<li><a href="{{ route('contato') }}">Contato</a></li>
		</ul>
	</div>

	<div class="col-md-12 menu-base">
		<form class="navbar-form">
			<label>Newsletter</label>
			<div class="input-group">
				<input type="text" class="form-control" placeholder="Cadastre seu email...">
				<div class="input-group-btn">
					<button class="btn btn-default" type="submit"><i class="icone-seta-direita"></i></button>
				</div>
			</div>
		</form>
		<ul class="nav menu-sec">
			<li><a href="{{ route('imprensa.index') }}">Imprensa</a></li></li>
			<li><a href="#">Blog Rio Moda</a></li></li>
			<li><a href="#">Frentes futuras</a></li></li>
			<li><a href="#">Oficinas de desenvolvimento</a></li>
		</ul>
	</div>
</div>