<a class="visible-xs bt-menu-oculto" data-toggle="offcanvas"><i class="icone-seta-esquerda"></i> Menu</a>

<div class="row">
	<div class="col-md-12 topo">
		<ul class="menu-rs">
			<li><a href="javascript:;" class="facebook"></a></li>
			<li><a href="javascript:;" class="instagram"></a></li>
			<li><a href="javascript:;" class="youtube"></a></li>
		</ul>
		<ul class="menu-right">
			<a href="{{ route('home') }}" class="visible-xs logo">
				<img src="{{asset('images/logo-irm.png')}}" class="img-responsive"/>
			</a>
			<li class="col-md-3 pesquisa">
				<div class="input-group">
					<input type="text" class="form-control" placeholder="Pesquisar...">
					<div class="input-group-btn">
						<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
					</div>
				</div>
			</li>
			@if(Sentry::check())
				<li>Olá, {{ Sentry::getUser()->username }}, 
					<a href="{{ route('user.edit') }}">Meus dados</a>
					<span>|</span>
					<a href="{{ route('user.events') }}">Eventos</a>
					<span>|</span>
					<a href="{{ route('user.logout') }}">Sair</a>
				</li>
			@else
			<li><a href="{{ route('user.register') }}">Cadastre-se</a></li>
			<li class="dropdown" id="menuLogin">
				<a class="dropdown-toggle" href="#" data-toggle="dropdown" id="navLogin">Login <i class="icone-seta-baixo right"></i></a>
				<div class="dropdown-menu" style="padding:17px;">
					{{ Form::open(['route'=>'user.login', 'id'=>'formLogin']) }}
						<input name="email" id="email" class="normal" type="text" placeholder="Email" title="Preencha o seu email" required="">
						<div class="input-group">
							<input name="password" id="password" type="password" placeholder="Senha" title="Preencha sua senha" required="">
							<div class="input-group-btn">
								<button class="btn btn-default" type="submit"><i class="icone-seta-direita"></i></button>
							</div>
						</div>
						
					{{ Form::close() }}
					<a data-toggle="modal" role="button" href="#forgotPasswordModal">Esqueceu sua senha?</a>
				</div>
			</li>
			@endif
			
		</ul>
	</div>
</div> <!-- /row -->