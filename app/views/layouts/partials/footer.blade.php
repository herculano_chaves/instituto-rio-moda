<footer>
	<div class="row">
		<div class="apoiadores espaco-banners">
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<a href="#" title="Apoiadores">
					<img src="{{ asset('images/footer/apoio1.jpg') }}" class="img-responsive" />
				</a>
			</div>
			<div class="col-md-2">
				<a href="#" title="Apoiadores">
					<img src="{{ asset('images/footer/apoio2.jpg') }}" class="img-responsive" />
				</a>
			</div>
			<div class="col-md-2">
				<a href="#" title="Apoiadores">
					<img src="{{ asset('images/footer/apoio3.jpg') }}" class="img-responsive" />
				</a>
			</div>
			<div class="col-md-2">
				<a href="#" title="Apoiadores">
					<img src="{{ asset('images/footer/apoio4.jpg') }}" class="img-responsive" />
				</a>
			</div>
			<div class="col-md-2">
				<a href="#" title="Apoiadores">
					<img src="{{ asset('images/footer/apoio5.jpg') }}" class="img-responsive" />
				</a>
			</div>
			<div class="col-md-1"></div>
		</div> 
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-12">
			<hr>
		</div>
	</div> <!-- /row -->

	<div class="row">
		<div class="espaco-banners">
			<div class="col-md-12">
				<ul class="left">
					<li><a href="#">Política de privacidade</a></li>
					<li><a href="#">Termos de uso e condições</a></li>
					<li><a href="#">Minha conta</a></li>
				</ul>
				<div class="pagamento right">
					<p class="left">Formas de pagamento:</p>
					<img src="{{ asset('images/footer/formas-de-pagamento.jpg') }}" class="img-responsive right" />
				</div>																
			</div>		

			<div class="col-md-12">
				<div class="direitos left">
					<p>&copy; 2008 Instituto Rio Moda | Todos os direitos reservados &bull; all rights reserved</p>
				</div>
				<div class="creditos right">
					<p>Desenvolvido por Diz'ain</p>
				</div>
			</div>
		</div>
	</div> <!-- /row -->
</footer>