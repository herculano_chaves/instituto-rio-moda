        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ route('admin.eventos.index') }}"><img height="30" src="{{asset('images/logo-irm.png')}}" alt=""></a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
         
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> {{ Sentry::getUser()->username }} <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                       
                        <li class="divider"></li>
                        <li>
                            <a href="{{route('admin.logout')}}"><i class="fa fa-fw fa-power-off"></i> Sair</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="{{ route('admin.titulos.index') }}"><i class="fa fa-fw fa-pencil-square-o"></i> Eventos (nomes)</a>
                    </li>
                    <li class="active">
                        <a href="{{ route('admin.eventos.index') }}"><i class="fa fa-fw fa-flag-checkered"></i> Eventos</a>
                    </li>
                     <li class="active">
                        <a href="{{ route('admin.taxonomias.index') }}"><i class="fa fa-tags"></i> Taxonomias</a>
                    </li>
                    <li class="active">
                        <a href="{{ route('admin.areas.index') }}"><i class="fa fa-building-o"></i> Áreas</a>
                    </li>
                     <li>
                        <a href="{{ route('admin.instituicoes.index') }}"><i class="fa fa-fw fa-university"></i> Instituições</a>
                    </li>
                     <li>
                        <a href="{{ route('admin.facilitator.index') }}"><i class="fa fa-fw fa-user-plus"></i> Facilitadores</a>
                    </li>
                     <li>
                        <a href="{{ route('admin.imprensa.index') }}"><i class="fa fa-fw fa-book"></i> Imprensa</a>
                    </li>
                     <li>
                        <a href="{{ route('admin.clientes.index') }}"><i class="fa fa-fw fa-user"></i> Clientes</a>
                    </li>
                    <li>
                        <a href="{{ route('admin.banners.index') }}"><i class="fa fa-fw fa-book"></i> Banners</a>
                    </li>
                  
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>