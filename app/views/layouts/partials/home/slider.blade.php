<div class="bxslider">
	<!-- <div class="fs_loader"></div> -->

	@foreach($products_featured_1 as $p)

	<div class="slide">	
		<div class="grid">
			<figure class="effect-bubba destaque">
				<img src="{{ asset('uploads/eventos/'.$p->src) }}" width="100%" class="img-responsive" />
				<figcaption>
					<div class="box-txt">
						<h3>{{ Helper::Dia($p->date_ini, true) }} e {{ Helper::Dia($p->date_fin, true) }} de {{ Helper::mesExtenso($p->date_fin) }} &diams; {{ $p->category->name or null }}</h3>
						<h2>{{ $p->title->name or null }} @if(!empty($p->title->name) and !empty($p->name)) - @endif{{ $p->name or null }}</h2>
						<p>facilitado por <strong>{{ $p->facilitator->name or null }}</strong></p>							
						<a href="{{ route('events.show',$p->slug) }}" title="Isncreva-se no Workshop">Inscreva-se <i class="icone-seta-direita"></i></a>
					</div>
				</figcaption>	
			</figure>
		</div>
	</div> <!-- /slide -->
	@endforeach
	

</div> <!-- /slider -->
