<!DOCTYPE>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Instituto Rio Moda - Administração</title>
	{{ HTML::style('css/bootstrap.css') }}
	{{ HTML::style('css/admin.css') }}
</head>
<body>
	@yield('content')
</body>
</html>