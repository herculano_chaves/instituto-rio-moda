<!DOCTYPE>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Instituto Rio Moda - Administração</title>
	{{ HTML::style('css/bootstrap.css') }}
	{{ HTML::style('css/datepicker.css') }}
	{{ HTML::style('//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css') }}
	{{ HTML::style('css/admin.css') }}
</head>
<body>
	 <div id="wrapper">

		@include('layouts.partials.admin.navigation')
		
        <div id="page-wrapper">

            <div class="container-fluid">
				
				@yield('content')

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

     <!-- Bootstrap Core JavaScript -->
	{{ HTML::script('js/jquery.js') }}
	{{ HTML::script('js/bootstrap.min.js') }}
	{{ HTML::script('js/jquery-ui.min.js') }}
	{{ HTML::script('js/jquery.mask.js') }}
	{{ HTML::script('//cdn.ckeditor.com/4.4.7/standard/ckeditor.js')}}
	{{ HTML::script('js/admin.js') }}
</body>
</html>