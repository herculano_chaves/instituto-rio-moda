@extends('layouts.admin')
@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Clientes <small>({{ $users->getTotal() }})</small>
        </h1>

    </div>
</div>
<!-- /.row -->
<div class="col-md-12">
	<!--<div class="alert alert-info text-right">
		<a href="{{ route('admin.clientes.create') }}" class="btn btn-primary "><i class="fa fa-plus"></i> NOVO</a>
	</div>-->
	 @include('layouts.notifications')
	@if($users->count() > 0)
    <div class="table-responsive">
        <table class="table table-bordered table-hover table-striped">
			<tr>
				<th>Nome</th>
				<th>Email</th>
				<th>Tels</th>
				<th>Endereço</th>
				<th>Editar</th>
				<th>Apagar</th>
				
			</tr>
			@foreach($users as $u)
				<tr>
					<td>{{ $u->username }}</td>
					<td>{{ $u->email }}</td>
					<td>Tel: {{ $u->userdata->tel }} Cel: {{ $u->userdata->cel }}</td>
					<td>
						{{ $u->userdata->address }}, 
						{{ $u->userdata->number }}/
						{{ $u->userdata->complement }},
						{{ $u->userdata->city->name or null }}-
						{{ $u->userdata->city->state->uf or null}},
					</td>
					<td><a href="{{ route('admin.clientes.edit', ['id'=>$u->id]) }}"><i class="fa fa-edit"></i></a></td>
					<td>
						{{ Form::open(['route'=>array('admin.clientes.destroy', 'id'=>$u->id), 'method'=>'delete']) }}
						<button type="submit">
			                <i class="fa fa-trash"></i>
			            </button>
						
						{{ Form::close() }}
					</td>
				</tr>
			@endforeach
		</table>
		{{ $users->appends($_GET)->links() }}
	</div>
	@else
	<div class="alert alert-warning">
		<p><i class="fa fa-marker"></i> Ainda não temos conteúdo para esta área</p>
	</div>
	@endif
</div>
@stop