@section('content')
<div id="page-dados">
	<div class="row">
		<div class="col-md-12">
			@include('layouts.notifications')
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="titulo">
				<div class="txt">
					<h1>Meus dados</h1>
				</div>
			</div>
		</div>
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-6">
			<h2>Alterar senha</h2>
			<p>Enviaremos um e-mail com as informações e um link para o seu e-mail cadastrado.</p>
			{{ Form::open(['route'=>'user.password.update']) }}
				<div class="form-group">
					{{ Form::password('password', ['placeholder'=>'Crie sua nova senha', 'class'=>'form-control']) }}
				</div>
				<div class="form-group">
					{{ Form::password('password_confirmation', ['placeholder'=>'Confirme sua nova senha', 'class'=>'form-control']) }}
				</div>
				<button type="submit" class="bt-padrao right">Alterar <i class="icone-seta-direita"></i></button>
			{{ Form::close() }} <!-- /form -->
		</div>		
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-6">
			<h2>Editar minha conta</h2>
		
			<h3>Campos obrigatórios</h3>
			{{ Form::open(['route'=>'user.update', 'method'=>'put', 'class'=>'form-cadastro']) }}

				<div class="form-group">
					{{ Form::text('username', $user->username, ['placeholder'=>'Nome completo', 'class'=>'form-control']) }}
				</div>
				<div class="form-group">
					{{ Form::email('email', $user->email, ['placeholder'=>'E-mail', 'class'=>'form-control']) }}
				</div>
				<div class="form-inline form-group">
			    	<label class="radio-inline">
						@if($user->userdata->cpf != null)
						{{ Form::radio('document', 'cpf', true) }} CPF
						@endif
					</label>
					<label class="radio-inline">
						@if($user->userdata->cnpj != null)
						{{ Form::radio('document', 'cnpj') }} CNPJ
						@endif
					</label>
					{{ Form::text('cpf', $user->userdata->cpf, ['placeholder'=>'Digite seu CPF', 'class'=>'form-control']) }}					
					{{ Form::text('cnpj', $user->userdata->cnpj, ['placeholder'=>'Digite seu CNPJ', 'class'=>'form-control', 'style'=>'display:none;']) }}					
				</div>
				<div class="form-group">
					{{ Form::text('tel', $user->userdata->tel, ['placeholder'=>'Telefone', 'class'=>'form-control']) }}
				</div>
				
				<div class="form-group">
					
					{{ Form::select('institution_id', [''=>'Selecione Instituição conveniada']+$institutions, $user->userdata->institution_id, ['class'=>'form-control', 'style'=>'width:70%;']) }}
				</div>
				<div class="form-group">
					{{ Form::select('education_id', [''=>'Escolaridade']+$educations, $user->userdata->education_id, ['class'=>'form-control', 'style'=>'width:70%;']) }}
				</div>

				<div class="form-group">
					<p>É estudante?</p>
					<label class="radio-inline">
						<input type="radio" name="estudante" id="estudante-sim" @if($user->userdata->course != null) checked @endif value="sim"> Sim
					</label>
					<label class="radio-inline">
						<input type="radio" name="estudante" id="estudante-nao" @if($user->userdata->course == null) checked @endif value="não"> Não
					</label>
				</div>
				<div id="student_block" @if($user->userdata->course == null) style="display:none;"  @endif>
					<div class="form-group">
						
						{{ Form::text('course', $user->userdata->course, ['placeholder'=>'Curso', 'class'=>'form-control']) }}
					</div>
					<div class="form-group">
						
						{{ Form::text('institution', $user->userdata->institution, ['placeholder'=>'Instituição de ensino', 'class'=>'form-control']) }}
					</div>
					<div class="form-group">
						
						{{ Form::text('year_graduation', $user->userdata->year_graduation, ['placeholder'=>'Ano da graduação', 'class'=>'form-control']) }}

					</div>
					<div class="form-group">
						
						{{ Form::text('num_course_register', $user->userdata->num_course_register, ['placeholder'=>'Matrícula', 'class'=>'form-control']) }}
					</div>
				</div>

				<h3>Campos complementares</h3>
				<div class="form-group">
					{{ Form::text('cel', $user->userdata->cel, ['placeholder'=>'Celular', 'class'=>'form-control']) }}
				</div>
				<div class="form-group">
					{{ Form::text('birthday', Helper::ConverterBR($user->userdata->birthday, true), ['placeholder'=>'Data de nascimento', 'class'=>'form-control']) }}
				</div>
				<div class="form-group">
					{{ Form::text('rg', $user->userdata->rg, ['placeholder'=>'Identidade', 'class'=>'form-control']) }}
				</div>
				<div class="form-group">
					{{ Form::text('registar', $user->userdata->registar, ['placeholder'=>'Órgão expedidor', 'class'=>'form-control']) }}
				</div>
				<div class="form-group">
					{{ Form::text('site', $user->userdata->site, ['placeholder'=>'Site / Blog', 'class'=>'form-control']) }}
				</div>
		
				<div class="form-group">
					{{ Form::text('cep', $user->userdata->cep, ['placeholder'=>'CEP', 'class'=>'form-control']) }}
				</div>
				<div class="form-group">
					{{ Form::text('address', $user->userdata->address, ['placeholder'=>'Endereço', 'class'=>'form-control']) }}
				</div>
				<div class="form-group">
					{{ Form::text('number', $user->userdata->number, ['placeholder'=>'Número', 'class'=>'form-control']) }}
				</div>
				<div class="form-group">
					{{ Form::text('complement', $user->userdata->complement, ['placeholder'=>'Complemento', 'class'=>'form-control']) }}
				</div>
				<div class="form-group">
					{{ Form::text('district', $user->userdata->district, ['placeholder'=>'Bairro', 'class'=>'form-control']) }}
				</div>
				<div class="form-group">
					{{ Form::select('state_id', [''=>'UF']+$states, $user->userdata->state_id, ['class'=>'form-control', 'id'=>'state_id']) }}
				</div>
				<div class="form-group">
					{{ Form::select('city_id', [''=>'Selecione cidade']+$cities, $user->userdata->city_id, ['class'=>'form-control', 'id'=>'city_id']) }}
				</div>

				<button type="submit" class="bt-padrao right">Editar <i class="icone-seta-direita"></i></button>
			</form> <!-- /form -->
		</div>
	</div> <!-- /row -->
	
</div> <!-- /page -->
@stop