@section('content')
<div id="page-cadastro">
	<div class="row">
		<div class="col-md-12">
			@include('layouts.notifications')
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="titulo">
				<div class="txt">
					<h1>Cadastro / Login</h1>
				</div>
			</div>
		</div>
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-6">
			<h2>Já é cadastrado?</h2>
			<p>Faça seu login para continuar.</p>			
			{{ Form::open(['route'=>'user.login', 'class'=>'form-login']) }}
				<div class="form-group">
					{{ Form::email('email', null, ['placeholder'=>'Digite seu e-mail', 'class'=>'form-control']) }}
				</div>
				<div class="form-group">
					{{ Form::password('password', ['placeholder'=>'Digite sua senha','class'=>'form-control']) }}
				</div>
				<button type="submit" class="bt-padrao right">Entrar <i class="icone-seta-direita"></i></button>
				<a href="#">Esqueci minha senha</a>
			{{ Form::close() }} <!-- /form -->
		</div>		
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-12">
			<hr>
		</div>
	</div>

	<div class="row">
		<div class="col-md-6">
			<h2>Ainda não é cadastrado?</h2>
			<p>Cadastre-se para inscrever-se em nossas atividades. É rápido e fácil.</p>
			<h3>Campos obrigatórios</h3>
			{{ Form::open(['route'=>'user.register.post', 'class'=>'form-cadastro']) }}

				<div class="form-group">
					{{ Form::text('name', null, ['placeholder'=>'Nome completo', 'class'=>'form-control']) }}
				</div>
				<div class="form-group">
					{{ Form::email('email', null, ['placeholder'=>'E-mail', 'class'=>'form-control']) }}
				</div>
				<div class="form-inline form-group">
			    	<label class="radio-inline">
						
						{{ Form::radio('document', 'cpf', true) }} CPF
					</label>
					<label class="radio-inline">
						{{ Form::radio('document', 'cnpj', false, ['style'=>'display:none;']) }} CNPJ
					</label>
					{{ Form::text('cpf', null, ['placeholder'=>'Digite seu CPF', 'class'=>'form-control']) }}					
					{{ Form::text('cnpj', null, ['placeholder'=>'Digite seu CNPJ', 'class'=>'form-control', 'style'=>'display:none;']) }}					
				</div>
				<div class="form-group">
					{{ Form::text('tel', null, ['placeholder'=>'Telefone', 'class'=>'form-control']) }}
				</div>
				
				<div class="form-group">
					
					{{ Form::password('password', ['placeholder'=>'Crie sua senha', 'class'=>'form-control']) }}
				</div>
				<div class="form-group">
					{{ Form::password('password_confirmation', ['placeholder'=>'Confirme sua senha', 'class'=>'form-control']) }}
				</div>
				<div class="form-group">
					
					{{ Form::select('institution_id', [''=>'Selecione Instituição conveniada']+$institutions, null, ['class'=>'form-control', 'style'=>'width:70%;']) }}
				</div>
				<div class="form-group">
					{{ Form::select('education_id', [''=>'Escolaridade']+$educations, null, ['class'=>'form-control', 'style'=>'width:70%;']) }}
				</div>
				<div class="form-group">
					<select name="poll" style="width:70%" class="form-control">
						<option>Como ficou sabendo desta iniciativa?</option>
						<option value="Revista">Revista</option>
						<option value="Jornal">Jornal</option>
						<option value="Internet">Internet</option>
						<option value="E-mail">E-mail</option>
						<option value="Amigos / Conhecidos">Amigos / Conhecidos</option>
						<option value="Outros">Outros</option>
					</select>
				</div>
				<div class="form-group">
					<p>É estudante?</p>
					<label class="radio-inline">
						<input type="radio" name="estudante" id="estudante-sim" value="sim"> Sim
					</label>
					<label class="radio-inline">
						<input type="radio" name="estudante" id="estudante-nao" value="não"> Não
					</label>
				</div>
				<div id="student_block" style="display:none;">
					<div class="form-group">
						
						{{ Form::text('course', null, ['placeholder'=>'Curso', 'class'=>'form-control']) }}
					</div>
					<div class="form-group">
						
						{{ Form::text('institution', null, ['placeholder'=>'Instituição de ensino', 'class'=>'form-control']) }}
					</div>
					<div class="form-group">
						
						{{ Form::text('year_graduation', null, ['placeholder'=>'Ano da graduação', 'class'=>'form-control']) }}

					</div>
					<div class="form-group">
						
						{{ Form::text('num_course_register', null, ['placeholder'=>'Matrícula', 'class'=>'form-control']) }}
					</div>
				</div>

				<h3>Campos complementares</h3>
				<div class="form-group">
					{{ Form::text('cel', null, ['placeholder'=>'Celular', 'class'=>'form-control']) }}
				</div>
				<div class="form-group">
					{{ Form::text('birthday', null, ['placeholder'=>'Data de nascimento', 'class'=>'form-control']) }}
				</div>
				<div class="form-group">
					{{ Form::text('rg', null, ['placeholder'=>'Identidade', 'class'=>'form-control']) }}
				</div>
				<div class="form-group">
					{{ Form::text('registar', null, ['placeholder'=>'Órgão expedidor', 'class'=>'form-control']) }}
				</div>
				<div class="form-group">
					{{ Form::text('site', null, ['placeholder'=>'Site / Blog', 'class'=>'form-control']) }}
				</div>
		
				<div class="form-group">
					{{ Form::text('cep', null, ['placeholder'=>'CEP', 'class'=>'form-control']) }}
				</div>
				<div class="form-group">
					{{ Form::text('address', null, ['placeholder'=>'Endereço', 'class'=>'form-control']) }}
				</div>
				<div class="form-group">
					{{ Form::text('number', null, ['placeholder'=>'Número', 'class'=>'form-control']) }}
				</div>
				<div class="form-group">
					{{ Form::text('complement', null, ['placeholder'=>'Complemento', 'class'=>'form-control']) }}
				</div>
				<div class="form-group">
					{{ Form::text('district', null, ['placeholder'=>'Bairro', 'class'=>'form-control']) }}
				</div>
				<div class="form-group">
					{{ Form::select('state_id', [''=>'UF']+$states, null, ['class'=>'form-control', 'id'=>'state_id']) }}
				</div>
				<div class="form-group">
					{{ Form::select('city_id', [''=>'Selecione cidade'], null, ['class'=>'form-control', 'id'=>'city_id']) }}
				</div>

				<button type="submit" class="bt-padrao right">Cadastrar <i class="icone-seta-direita"></i></button>
			</form> <!-- /form -->
		</div>
	</div> <!-- /row -->
	
</div> <!-- /page -->
@stop