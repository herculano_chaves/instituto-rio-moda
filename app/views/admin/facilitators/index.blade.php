@extends('layouts.admin')
@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Facilitadores
        </h1>
    </div>
</div>
<!-- /.row -->
<div class="col-md-12">
	<div class="alert alert-info text-right">
		<a href="{{ route('admin.facilitator.create') }}" class="btn btn-primary "><i class="fa fa-plus"></i> NOVO</a>
	</div>
	 @include('layouts.notifications')
	@if($facilitators->count() > 0)
    <div class="table-responsive">
        <table class="table table-bordered table-hover table-striped">
			<tr>
				<th>Imagem</th>
				<th>Nome</th>
				<th>Conteúdo</th>
				<th>Editar</th>
				<th>Apagar</th>
				
			</tr>
			@foreach($facilitators as $f)
				<tr>
					<td>{{ isset($f->src) ? HTML::image('uploads/'.$f->src, $f->name, ['width'=>90]) : '<img src="http://placehold.it/90x90">' }}</td>
					<td>{{ $f->name }}</td>
					<td>{{ str_limit($f->content, 100, '...') }}</td>
					<td><a href="{{ route('admin.facilitator.edit', ['id'=>$f->id]) }}"><i class="fa fa-edit"></i></a></td>
					<td>
						{{ Form::open(['route'=>array('admin.facilitator.destroy', 'id'=>$f->id), 'method'=>'delete']) }}
						<button type="submit">
			                <i class="fa fa-trash"></i>
			            </button>
						
						{{ Form::close() }}
					</td>
				</tr>
			@endforeach
		</table>
		{{ $facilitators->appends($_GET)->links() }}
	</div>
	@else
	<div class="alert alert-warning">
		<p><i class="fa fa-marker"></i> Ainda não temos conteúdo para esta área</p>
	</div>
	@endif
</div>
@stop