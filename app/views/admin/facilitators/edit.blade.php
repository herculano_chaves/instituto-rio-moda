@extends('layouts.admin')
@section('content')
	
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Facilitador<small>Editar</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-user-plus"></i> {{ $facilitator->name }}
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-md-8">
                        
                        @include('layouts.notifications')

                        {{ Form::open(['route'=>array('admin.facilitator.update', 'id'=>$facilitator->id), 'files'=>true, 'method' => 'put']) }}
                        <table class="table table-bordered table-striped">
                            <tr>
                                <td>Imagem</td>
                                <td>{{ HTML::image('uploads/'.$facilitator->src, $facilitator->name, ['width'=>90])  }}
                                    {{ Form::file('src') }}</td>
                            </tr>
                            <tr>
                                <td>Nome</td>
                                <td>{{ Form::text('name', $facilitator->name, ['class'=>'form-control']) }}</td>
                            </tr>
                             
                              <tr>
                                <td>Conteúdo</td>
                                <td>{{ Form::textarea('content', $facilitator->content, ['class'=>'form-control ckeditor'] ) }}</td>
                            </tr>
                            <tr>
                                <td colspan="2" class="text-right">
                                    {{ Form::submit('Enviar', ['class'=>'btn btn-primary']) }}
                                </td>
                            </tr>
                        </table>
                        {{ Form::close() }}
                    </div>
                </div>
@stop