@extends('layouts.admin')
@section('content')
	
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Facilitadores <small>Criar novo</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-user-plus"></i> Facilitador
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-md-8">
                        
                        @include('layouts.notifications')

                        {{ Form::open(['route'=>'admin.facilitator.store', 'files'=>true]) }}
                        <table class="table table-bordered table-striped">
                            <tr>
                                <td>Imagem</td>
                                <td>{{ Form::file('src') }}</td>
                            </tr>
                            <tr>
                                <td>Nome</td>
                                <td>{{ Form::text('name', null, ['class'=>'form-control']) }}</td>
                            </tr>
                             
                              <tr>
                                <td>Conteúdo</td>
                                <td>{{ Form::textarea('content', null, ['class'=>'form-control ckeditor'] ) }}</td>
                            </tr>
                            <tr>
                                <td colspan="2" class="text-right">
                                    {{ Form::submit('Enviar', ['class'=>'btn btn-primary']) }}
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
@stop