@extends('layouts.admin')
@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Banners
        </h1>
    </div>
</div>
<!-- /.row -->
<div class="col-md-12">
	<div class="alert alert-info text-right">
		<a href="{{ route('admin.banners.create') }}" class="btn btn-primary "><i class="fa fa-plus"></i> NOVA</a>
	</div>
	 @include('layouts.notifications')
	@if($banners->count() > 0)
    <div class="table-responsive">
        <table class="table table-bordered table-hover table-striped">
			<tr>
				<th>Imagem</th>
				<th>Nome</th>
				<th>Destaque</th>
				<th>Link</th>
				<th>Editar</th>
				<th>Apagar</th>
				
			</tr>
			@foreach($banners as $f)
				<tr>
					<td>{{ HTML::image('uploads/banners/'.$f->src, $f->name, ['width'=>90]) }}</td>
					<td>{{ $f->name }}</td>
					<td>{{ $f->featured }}</td>
					<td>{{ $f->link }}</td>
					
				
					
					<td><a href="{{ route('admin.banners.edit', ['id'=>$f->id]) }}"><i class="fa fa-edit"></i></a></td>
					
					<td>
						{{ Form::open(['route'=>array('admin.banners.destroy', 'id'=>$f->id), 'method'=>'delete']) }}
						<button type="submit">
			                <i class="fa fa-trash"></i>
			            </button>
						
						{{ Form::close() }}
					</td>
				</tr>
			@endforeach
		</table>
		{{ $banners->appends($_GET)->links() }}
	</div>
	@else
	<div class="alert alert-warning">
		<p><i class="fa fa-marker"></i> Ainda não temos conteúdo para esta área</p>
	</div>
	@endif
</div>
@stop