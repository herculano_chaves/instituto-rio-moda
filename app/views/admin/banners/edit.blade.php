@extends('layouts.admin')
@section('content')
	
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Banners <small>Editar</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-tags"></i> Banners
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-md-8">
                        
                        @include('layouts.notifications')

                        {{ Form::open(['route'=>array('admin.banners.update',$banner->id), 'method'=>'put', 'files'=>true]) }}
                        <table class="table table-bordered table-striped">
                            <tr>
                                <td>Imagem</td>
                                <td>
                                    <img src="{{ asset('uploads/banners/'.$banner->src) }}" width="90" class="img-responsive" />
                                    {{ Form::file('src') }}</td>
                            </tr>
                            <tr>
                                <td>Nome</td>
                                <td>{{ Form::text('name', $banner->name, ['class'=>'form-control']) }}</td>
                            </tr>
                             
                             <tr>
                                <td>Destaque (não obrigatório)</td>
                                <td>
                                    {{ Form::radio('featured', 1) }} Banner Principal Home 
                                    {{ Form::radio('featured', 2) }} Banner destaque
                                    {{ Form::radio('featured', 3) }} Destaque
                                </td>
                            </tr>
                            <tr>
                                <td>Link</td>
                                <td>{{ Form::text('link',$banner->link, ['class'=>'form-control']) }}</td>
                            </tr>
                             <tr>
                                <td>Ativo</td>
                                <td>{{ Form::checkbox('active', 1, true) }}</td>
                            </tr>
                             
                            <tr>
                                <td colspan="2" class="text-right">
                                    {{ Form::submit('Enviar', ['class'=>'btn btn-primary']) }}
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
@stop