@extends('layouts.login')
@section('content')
	<div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
            	<h1 class="text-center"><img src="{{asset('images/logo-irm.png')}}" alt="Instituto Rio Moda - Login"></h1>
            	@include('layouts.notifications')
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Login no sistema</h3>
                    </div>
                    <div class="panel-body">
                        {{ Form::open(['route'=>'admin.login']) }}
                            <fieldset>
                                <div class="form-group">
                                   {{ Form::email('email', null, ['placeholder'=>'E-mail', 'class'=>'form-control']) }}
                                </div>
                                <div class="form-group">
                                   {{ Form::password('password', ['placeholder'=>'Senha', 'class'=>'form-control']) }}
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="Remember Me">Remember Me
                                    </label>
                                </div>
                               
                                {{ Form::submit('Login', ['class'=>'btn btn-lg btn-success btn-block']) }}
                                
                            </fieldset>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop