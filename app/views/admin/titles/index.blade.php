@extends('layouts.admin')
@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Títulos de eventos
        </h1>
    </div>
</div>
<!-- /.row -->
<div class="col-md-12">
	<div class="alert alert-info text-right">
		<a href="{{ route('admin.titulos.create') }}" class="btn btn-primary "><i class="fa fa-plus"></i> NOVO</a>
	</div>
	 @include('layouts.notifications')
	@if($titles->count() > 0)
    <div class="table-responsive">
        <table class="table table-bordered table-hover table-striped">
			<tr>
				
				<th>Nome</th>
				
				<th>Editar</th>
				<th>Apagar</th>
				
			</tr>
			@foreach($titles as $t)
				<tr>
				
					<td>{{ $t->name }}</td>
				
					<td><a href="{{ route('admin.titulos.edit', ['id'=>$t->id]) }}"><i class="fa fa-edit"></i></a></td>
					<td>
						{{ Form::open(['route'=>array('admin.titulos.destroy', 'id'=>$t->id), 'method'=>'delete']) }}
						<button type="submit">
			                <i class="fa fa-trash"></i>
			            </button>
						
						{{ Form::close() }}
					</td>
				</tr>
			@endforeach
		</table>
		{{ $titles->appends($_GET)->links() }}
	</div>
	@else
	<div class="alert alert-warning">
		<p><i class="fa fa-marker"></i> Ainda não temos conteúdo para esta área</p>
	</div>
	@endif
</div>
@stop