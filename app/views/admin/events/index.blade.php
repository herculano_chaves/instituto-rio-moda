@extends('layouts.admin')
@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Eventos <small>({{ $events->getTotal() }})</small>
        </h1>
    </div>
</div>
<!-- /.row -->
<div class="col-md-12">
	<div class="alert alert-info text-right">
		<a href="{{ route('admin.eventos.create') }}" class="btn btn-primary "><i class="fa fa-plus"></i> NOVO</a>
	</div>
	
	 @include('layouts.notifications')

	@if($events->count() > 0)
    <div class="table-responsive">
        <table class="table table-bordered table-hover table-striped">
			<tr>
				<th>Imagem</th>
				<th>Título</th>
				<th>Tipo</th>
				<th>Endereço</th>
				<th>Datas/Horários</th>
				<th>Ativo</th>
				<th>Editar</th>
				<th>Apagar</th>
			</tr>
			@foreach($events as $e)
			<tr>			
				<td>{{ HTML::image('uploads/eventos/'.$e->src, $e->name, ['height'=>80]) }}</td>
				<td>{{ $e->name }}</td>
				<td>{{ $e->category->name }}</td>
				<td>{{ $e->address }}, {{ $e->number }} {{ $e->complement or null }}, {{ $e->district }}, {{ $e->city->name or null }} - {{ $e->city->state->uf or null }}</td>
				<td>
					@if($e->date_ini != '')
					{{ Helper::ConverterBR($e->date_ini) }} até {{ Helper::ConverterBR($e->date_fin) }} das {{ $e->hour }}
					@endif
				</td>
				<td>{{ $e->active == '1' ? 'Sim' : 'Não' }}</td>
				<td>
					<a href="{{ route('admin.eventos.edit', ['id'=>$e->id]) }}"><i class="fa fa-edit"></i></a>
				</td>
				<td>
					{{ Form::open(['route'=>array('admin.eventos.destroy', 'id'=>$e->id), 'method'=>'delete']) }}
					<button type="submit">
		                <i class="fa fa-trash"></i>
		            </button>
					
					{{ Form::close() }}
				</td>
			</tr>
			@endforeach
		</table>
		{{ $events->appends($_GET)->links() }}
	</div>
	@else
	<div class="alert alert-warning">
		<p><i class="fa fa-marker"></i> Ainda não temos conteúdo para esta área</p>
	</div>
	@endif
</div>
@stop