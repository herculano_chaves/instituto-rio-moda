@extends('layouts.admin')
@section('content')
    
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Eventos <small>Editar</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> {{ $event->name }}
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-md-8">
                        
                        @include('layouts.notifications')

                        {{ Form::open(['route'=>array('admin.eventos.update', $event->id), 'method'=>'put', 'files'=>true]) }}
                        <table class="table table-bordered table-striped">
                            <tr>
                                <td>Imagem</td>
                                <td>
                                    {{ HTML::image('uploads/eventos/thumb/'.$event->src) }} 
                                    {{ Form::file('src') }}</td>
                            </tr>
                            <tr>
                                <td>Destaque (não obrigatório)</td>
                                <td>
                                    {{ Form::radio('featured', 4, $check_1) }} Hiper-destaque 
                                    {{ Form::radio('featured', 2, $check_2) }} Mega-destaque 
                                    {{ Form::radio('featured', 3, $check_3) }} Destaque
                                </td>
                            </tr>
                             <tr>
                                <td>Nome *</td>
                                <td>{{ Form::select('title_id', [''=>'Selecione']+$titles, $event->title_id, ['class'=>'form-control']) }}</td>
                            </tr>
                        
                             <tr>
                                <td>Nome Complementar</td>
                                <td>{{ Form::text('name', $event->name, ['class'=>'form-control']) }}</td>
                            </tr>
                             <tr>
                                <td>Tipologia *</td>
                                <td>{{ Form::select('category_id', [''=>'Selecione']+$categories, $event->category_id, ['class'=>'form-control']) }}</td>
                            </tr>
                             <tr>
                                <td>Facilitador *</td>
                                <td>{{ Form::select('facilitator_id', [''=>'Selecione']+$facilitators, $event->facilitator_id, ['class'=>'form-control']) }}</td>
                            </tr>
                             <tr>
                                <td>Local</td>
                                <td>
                                    <table class="table">
                                        <tr>
                                            <td>Endereço *</td>
                                            <td>{{ Form::text('address', $event->address, ['class'=>'form-control']) }}</td>
                                        </tr>
                                         <tr>
                                            <td>Numero *</td>
                                            <td>{{ Form::number('number', $event->number, ['class'=>'form-control']) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Complemento *</td>
                                            <td>{{ Form::text('complement', $event->complement, ['class'=>'form-control']) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Estado *</td>
                                            <td>{{ Form::select('state_id', [''=>'UF']+$states, $event->city->state->id, ['class'=>'form-control', 'id'=>'state_id']) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Cidade *</td>
                                            <td>{{ Form::select('city_id', [''=>'Selecione cidade']+$cities, $event->city_id, ['class'=>'form-control', 'id'=>'city_id']) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Bairro *</td>
                                            <td>{{ Form::text('district', $event->district, ['class'=>'form-control']) }}</td>
                                        </tr>
                                      
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>Data início *</td>
                                <td>{{ Form::text('date_ini', Helper::ConverterBR($event->date_ini), ['class'=>'form-control datepicker'] ) }}</td>
                            </tr>
                             <tr>
                                <td>Data término *</td>
                                <td>{{ Form::text('date_fin', Helper::ConverterBR($event->date_fin), ['class'=>'form-control datepicker'] ) }}</td>
                            </tr>
                             <tr>
                                <td>Hora *</td>
                                <td>{{ Form::text('hour', $event->hour, ['class'=>'form-control'] ) }}</td>
                            </tr>
                             <tr>
                                <td>Duração *</td>
                                <td>{{ Form::text('duration', $event->duration, ['class'=>'form-control'] ) }}</td>
                            </tr>
                             <tr>
                                <td>Data limite de inscrição</td>
                                <td>{{ Form::text('date_event_register_limit', Helper::ConverterBR($event->date_event_register_limit), ['class'=>'form-control'] ) }}</td>
                            </tr>
                              <tr>
                                <td>Preço *</td>
                                <td>{{ Form::text('price', $event->price, ['class'=>'form-control'] ) }}</td>
                            </tr>
                            <tr>
                                <td>Conteúdo *</td>
                                <td>{{ Form::textarea('content', $event->content, ['class'=>'form-control ckeditor'] ) }}</td>
                            </tr>
                            <tr>
                                <td>Informação de desconto</td>
                                <td>{{ Form::textarea('info_discount', $event->info_discount, ['class'=>'form-control'] ) }}</td>
                            </tr>
                             <tr>
                                <td>Currículo anexo *</td>
                                <td>{{ Form::file('curriculum') }}</td>
                            </tr>
                            <tr>
                                <td>Eventos relacionados *</td>
                                <td>{{ Form::select('related_events[]', $events, $related_events, ['class'=>'form-control', 'multiple', 'style'=>'height:200px;']) }}</td>
                            </tr>
                            <tr>
                                <td>Ativo</td>
                                <td>{{ Form::checkbox('active', 1, true) }}</td>
                            </tr>
                            <tr>
                                <td colspan="2" class="text-right">
                                    {{ Form::submit('Enviar', ['class'=>'btn btn-primary']) }}
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
@stop