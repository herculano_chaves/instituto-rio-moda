@extends('layouts.admin')
@section('content')
	
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Imprensa <small>Editar</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-book"></i> {{$notice->title}}
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-md-8">
                        
                        @include('layouts.notifications')

                        {{ Form::open(['route'=> array('admin.imprensa.update',$notice->id), 'method'=>'put', 'files'=>true]) }}
                        <table class="table table-bordered table-striped">
                            <tr>
                                <td>Imagem</td>
                                <td>
                                    {{ HTML::image('uploads/imprensa/thumb/'.$notice->src, $notice->name, ['width'=>90])}}
                                    {{ Form::file('src') }}</td>
                            </tr>
                            <tr>
                                <td>Título</td>
                                <td>{{ Form::text('title', $notice->title, ['class'=>'form-control']) }}</td>
                            </tr>
                             <tr>
                                <td>Chamada</td>
                                <td>{{ Form::textarea('lead', $notice->lead, ['class'=>'form-control ckeditor']) }}</td>
                            </tr>
                             
                              <tr>
                                <td>Conteúdo</td>
                                <td>{{ Form::textarea('content', $notice->content, ['class'=>'form-control ckeditor'] ) }}</td>
                            </tr>
                            <tr>
                                <td colspan="2" class="text-right">
                                    {{ Form::submit('Enviar', ['class'=>'btn btn-primary']) }}
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
@stop