@extends('layouts.admin')
@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Imprensa
        </h1>
    </div>
</div>
<!-- /.row -->
<div class="col-md-12">
	<div class="alert alert-info text-right">
		<a href="{{ route('admin.imprensa.create') }}" class="btn btn-primary "><i class="fa fa-plus"></i> NOVO</a>
	</div>
	 @include('layouts.notifications')
	@if($notices->count() > 0)
    <div class="table-responsive">
        <table class="table table-bordered table-hover table-striped">
			<tr>
				<th>Imagem</th>
				<th>Nome</th>
				<th>Chamada</th>
				<th>Conteúdo</th>
				<th>Editar</th>
				<th>Apagar</th>
				
			</tr>
			@foreach($notices as $n)
				<tr>
					<td>{{ isset($n->src) ? HTML::image('uploads/imprensa/thumb/'.$n->src, $n->name, ['width'=>90]) : '<img src="http://placehold.it/90x90">' }}</td>
					<td>{{ $n->title }}</td>
					<td>{{ $n->lead }}</td>
					<td>{{ str_limit($n->content, 100, '...') }}</td>
					<td><a href="{{ route('admin.imprensa.edit', ['id'=>$n->id]) }}"><i class="fa fa-edit"></i></a></td>
					<td>
						{{ Form::open(['route'=>array('admin.imprensa.destroy', 'id'=>$n->id), 'method'=>'delete']) }}
						<button type="submit">
			                <i class="fa fa-trash"></i>
			            </button>
						
						{{ Form::close() }}
					</td>
				</tr>
			@endforeach
		</table>
		{{ $notices->appends($_GET)->links() }}
	</div>
	@else
	<div class="alert alert-warning">
		<p><i class="fa fa-marker"></i> Ainda não temos conteúdo para esta área</p>
	</div>
	@endif
</div>
@stop