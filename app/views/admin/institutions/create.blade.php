@extends('layouts.admin')
@section('content')
	
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Instituições <small>Criar novo</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-university"></i> Instituições
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-md-8">
                        
                        @include('layouts.notifications')

                        {{ Form::open(['route'=>'admin.instituicoes.store']) }}
                        <table class="table table-bordered table-striped">
                         
                            <tr>
                                <td>Nome</td>
                                <td>{{ Form::text('name', null, ['class'=>'form-control']) }}</td>
                            </tr>
                             <tr>
                                <td>Desconto</td>
                                <td>{{ Form::number('discount', null, ['class'=>'form-control']) }}</td>
                            </tr>
                             <tr>
                                <td>Ativo?</td>
                                <td>{{ Form::checkbox('active', 1) }}</td>
                            </tr>
                             
                            <tr>
                                <td colspan="2" class="text-right">
                                    {{ Form::submit('Enviar', ['class'=>'btn btn-primary']) }}
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
@stop