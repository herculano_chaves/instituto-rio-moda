@extends('layouts.admin')
@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Instituições
        </h1>
    </div>
</div>
<!-- /.row -->
<div class="col-md-12">
	<div class="alert alert-info text-right">
		<a href="{{ route('admin.instituicoes.create') }}" class="btn btn-primary "><i class="fa fa-plus"></i> NOVO</a>
	</div>
	 @include('layouts.notifications')
	@if($institutions->count() > 0)
    <div class="table-responsive">
        <table class="table table-bordered table-hover table-striped">
			<tr>
				<th>Nome</th>
				<th>Desconto</th>
				<th>Ativo</th>
				<th>Editar</th>
				<th>Apagar</th>
				
			</tr>
			@foreach($institutions as $i)
				<tr>
					
					<td>{{ $i->name }}</td>
					<td>{{ $i->discount }}%</td>
					<td>{{ $i->active == '1' ? '<i class="fa fa-check"></i>' : '<i class="fa fa-times"></i>' }}</td>
				
					<td><a href="{{ route('admin.instituicoes.edit', ['id'=>$i->id]) }}"><i class="fa fa-edit"></i></a></td>
					<td>
						{{ Form::open(['route'=>array('admin.instituicoes.destroy', 'id'=>$i->id), 'method'=>'delete']) }}
						<button type="submit">
			                <i class="fa fa-trash"></i>
			            </button>
						
						{{ Form::close() }}
					</td>
				</tr>
			@endforeach
		</table>
		{{ $institutions->appends($_GET)->links() }}
	</div>
	@else
	<div class="alert alert-warning">
		<p><i class="fa fa-marker"></i> Ainda não temos conteúdo para esta área</p>
	</div>
	@endif
</div>
@stop