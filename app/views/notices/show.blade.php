@section('content')
<div id="page-imprensa">

	<div class="row">
		<div class="col-md-12">
			<div class="titulo">
				<div class="txt">
					<h1>Imprensa</h1>
				</div>
			</div>
		</div>
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-6 right">
			{{ HTML::image('uploads/imprensa/'.$notice->src, $notice->title, ['class'=>'img-responsive right']) }}
		</div>

		<div class="col-md-6 left">
			<h2>{{ $notice->title }}</h2>
			{{ $notice->content }}
		</div>		
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-12">
			<div class="barra-opcoes">
				<a href="{{ route('imprensa.index') }}" class="left"><i class="glyphicon glyphicon-th"></i> Voltar para listagem</a>

				<a href="#" class="right"><i class="glyphicon glyphicon-print"></i> Imprimir</a>
				<a href="#" class="right"><i class="glyphicon glyphicon-share"></i> Enviar para um amigo</a>
			</div> <!-- /barra -->
		</div>	
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-6 right">
			<img src="{{ asset('images/imprensa/banner.jpg') }}" width="261" height="190" class="img-responsive right">
		</div>
		<div class="col-md-6 left">
			<h2>O Instituto Rio Moda disponibiliza release sobre suas atividades.</h2>
			<a href="#" class="bt-padrao clear" title="Baixar release">Baixar release <i class="icone-seta-direita"></i></a>
			<h3>Para receber fotos em alta resolução de todos os seus workshops e eventos, entre em contato:</h3>
			<p>&bull; riomoda@institutoriomoda.com.br</p>
			<p>&bull; (21) 2523-9975 e 2227-5094.</p>
			<h3>Acompanhe e divulgue a nossa programação.</h3>
		</div>		
	</div>
	
</div> <!-- /page -->
@stop