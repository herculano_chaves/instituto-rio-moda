@section('content')
<div id="page-imprensa">

	<div class="row">
		<div class="col-md-12">
			<div class="titulo">
				<div class="txt">
					<h1>Imprensa</h1>
				</div>
			</div>
		</div>
	</div> <!-- /row -->

	<div class="row">
		@foreach($notices as $n)
			<div class="col-md-4">
				<div class="socios imgs-quem-somos">
					<a href="{{ route('imprensa.show', $n->id) }}">
						{{ HTML::image('uploads/imprensa/thumb/'.$n->src, $n->title, ['class'=>'img-responsive']) }}
					</a>
					<h2>
						<a href="{{ route('imprensa.show', $n->id) }}">{{ $n->title }}</a>
					</h2>
					<p>
						<a href="{{ route('imprensa.show', $n->id) }}">{{ $n->lead }}</a>
					</p>
				</div>
			</div>
		@endforeach
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-12">
			<div class="vejamais">
				<span>
					<a href="#" class="bt-next">ver mais</a>
				</span>
				<hr>
			</div>
		</div>
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-6 right">
			<img src="images/imprensa/banner.jpg" width="261" height="190" class="img-responsive right">
		</div>
		<div class="col-md-6 left">
			<h2>O Instituto Rio Moda disponibiliza release sobre suas atividades.</h2>
			<a href="#" class="bt-padrao clear" title="Baixar release">Baixar release <i class="icone-seta-direita"></i></a>
			<h3>Para receber fotos em alta resolução de todos os seus workshops e eventos, entre em contato:</h3>
			<p>&bull; riomoda@institutoriomoda.com.br</p>
			<p>&bull; (21) 2523-9975 e 2227-5094.</p>
			<h3>Acompanhe e divulgue a nossa programação.</h3>
		</div>		
	</div>
	
</div> <!-- /page -->
@stop