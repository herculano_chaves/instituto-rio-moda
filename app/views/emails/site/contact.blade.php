<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Contato site - Instituto Rio Moda</h2>

		<table>
			<tr>
				<td>Nome:</td>
				<td>{{ $data['name'] }}</td>
			</tr>
			<tr>
				<td>Email:</td>
				<td>{{ $data['email'] }}</td>
			</tr>
			<tr>
				<td>Assunto:</td>
				<td>{{ $data['subject'] }}</td>
			</tr>
			<tr>
				<td>Mensagem:</td>
				<td>{{ $data['message'] }}</td>
			</tr>
		</table>
	</body>
</html>
