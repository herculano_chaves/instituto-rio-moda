<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
		<style>
			td { padding: 0; }
		</style>
	</head>
	<body>
		<table width="595" align="center" cellspadding="0" cellspacing="0" style="border: 1px solid #ccc;">
			<!--
			<tr>
				<td><img src="{{URL::to('/')}}/images/emails/top.png" width="595" height="100" style="display:block; border:none;"></td>
			</tr>
		    -->
			<tr>
				<td><p style="font:18px Arial; color:#313131; margin: 40px 0 0 30px;"><b>Olá, {{ $user['name'] }}</b></p></td>
			</tr>
			<tr>
				<td><p style="font:18px Arial; color:#313131; margin: 10px 0 0 30px;">Você solicitou uma nova senha. Clique no link a seguir e será direcionado para a criação de uma nova senha.</p></td>
			</tr>
			<tr>
				<td><p style="font:18px Arial; color:#313131; margin: 10px 0 20px 30px;"><a href="{{url('password/reset/'.$resetCode)}}">{{url('password/reset/'.$resetCode)}}</a></p></td>
			</tr>
		
			<!--
			<tr>
				<td><img src="{{URL::to('/')}}/images/emails/bottom1.png" width="595" height="82" style="display:block; border:none;"></td>
			</tr>
			-->
		</table>
	</body>
</html>
