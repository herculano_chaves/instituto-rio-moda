@section('content')
	<div id="page-atividades">

	<div class="row">
		<div class="col-md-12">
			<div class="titulo">
				<div class="txt">
					<h1>Produtos</h1>
				</div>			
			</div>
		</div>
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-12">
			<h2>{{ $taxonomy->name }}</h2>
		</div>
	</div> <!-- /row -->

	<div class="row" style="position:relative;">
		<div class="col-md-12">
			<ul class="nav nav-tabs">
				@foreach($taxonomy->categories as $k=>$category)

			    	<li @if($k=='0') class="active" @endif><a data-toggle="tab" href="#{{$category->slug}}">{{ $category->name }}</a></li>

			    @endforeach
			   
		  	</ul>
		</div>
		<div class="filter_month">
			{{ Form::open() }}
			{{ Form::select('') }}
			{{ Form::close() }}
		</div>
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-12">
			<div class="tab-content">
			   @foreach($taxonomy->categories as $k => $category)
				   <div id="{{$category->slug}}" class="tab-pane fade @if($k=='0') in active @endif ">
				   		<div class="col-md-12">
				   			
				   		@foreach($events->chunk(4) as $chunk)
				   			
		   					<div class="row event-item">
							@foreach ($chunk as $event)
									<a href="{{route('events.show',$event->slug)}}">
										<div class="col-md-3" style="overflow:hidden;">
											<div style="width:100%;height:206px; background-position:center; background-size:cover;background-image:url('{{asset('uploads/eventos/thumb/'.$event->src)}}');"></div>
											
											<p>
												@if(isset($event->date_ini) or isset($event->date_fin))
												@if($event->date_ini == $event->date_fin)
													{{Helper::Dia($event->date_ini, true) }} 
												@else
												{{Helper::Dia($event->date_ini, true) }} 
												e {{ Helper::Dia($event->date_fin, true) }} 
												@endif
												de {{ Helper::mesExtenso($event->date_fin) }} 
												de {{ Helper::Ano($event->date_fin) }}
												@endif
											</p>
											<p>{{ $event->title->name or null }}</p>
											<h3>{{ $event->name }}</h3>
											{{ strip_tags(str_limit($event->content,100,'...')) }}
										</div>
									</a>
							@endforeach
					
							</div>
						@endforeach
						
				   </div>
				</div>
				@endforeach
					

			</div> <!-- /tab-content -->
		</div>
	</div> <!-- /row -->

	<div class="row">
		
	</div> <!-- /row -->

</div> <!-- /page -->
@stop