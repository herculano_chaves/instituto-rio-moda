@section('content')
	<div id="page-atividades">

	<div class="row">
		<div class="col-md-12">
			<div class="titulo">
				<div class="txt">
					<h1>Produtos</h1>
				</div>			
			</div>
		</div>
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-12">
			<h2>{{ $event->category->taxonomy->name }}</h2>
		</div>
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-12">
			<ul class="nav nav-tabs">
			   
			    <li class="active"><a data-toggle="tab" href="#workshops">{{ $event->category->name }}</a></li>
			  
		  	</ul>
		</div>
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-12">
			<div class="tab-content">

			    <div id="workshops" class="tab-pane fade in active">
			    	<div class="col-md-6 right">
						<img src="{{asset('uploads/eventos/thumb/'.$event->src)}}" alt="" class="img-responsive right">
						<div class="informacoes">
							<select class="bt-select">
								<option selected>Informações específicas</option>
								<option>X2</option>
								<option>X3</option>
								<option>X4</option>
								<option>X5</option>
							</select>
							<a href=""><i class="icone-pdf"></i></a>
						</div>
						{{ Form::open(['route'=>array('event.add', $event->id)]) }}
						{{ Form::hidden('id', $event->id) }}
						<button type="submit" class="bt-padrao right clear" title="Inscreva-se">Inscreva-se <i class="icone-seta-direita"></i></button>
						{{ Form::close() }}
					</div>
					<div class="col-md-6 left">
						<h3>{{ $event->title->name or null }} @if(!empty($event->title->name) and !empty($event->name)) - @endif{{ $event->name or null }}</h3>
						<p><span>Nome da atividade:</span> {{$event->title->name or null}}</p>
						<p><span>Duração:</span> {{$event->duration}}</p>
						@if($event->date_ini != null and $event->date_fin != null)
							<p><span>Data de Início:</span> {{ Helper::ConverterBR($event->date_ini) }}</p>
							<p><span>Data de Término:</span> {{ Helper::ConverterBR($event->date_fin) }}</p>
						@else
							<p><span>Data:</span> 
								@if($event->date_ini != null)
									{{ Helper::ConverterBR($event->date_ini) }}
								@else
									{{ Helper::ConverterBR($event->date_fin) }}
								@endif
							</p>
						@endif
						<p><span>Horário:</span> {{ $event->hour }}</p>
						<p><span>Conteúdo:</span>{{ $event->content }}</p>
						<p><span>Local:</span> {{ $event->address }}, {{ $event->number }} {{ $event->complement  }}, {{ $event->district }}, {{ $event->city->name or null }} - {{ $event->city->state->uf or null }}</p>
						<p><span>Preço:</span> R$ {{ $event->price }},00</p>
					</div>
					

					<div class="col-md-12 clear">
						<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingOne">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="collapsed">
											Confira os descontos
										</a>
									</h4>
								</div>
								<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
									<div class="panel-body">
										<h2>Preços por programas especiais e descontos</h2>
										<p>Todas as atividades do Rio Moda poderão ser pagas à vista ou parceladas em até 12 vezes no cartão. Aproveite e junte-se a nós em mais de uma atividade!</p>
										<table class="table">
											<thead>
												<tr>
													<th>Tipo de compra</th>
													<th>Valor (R$)</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>1 único workshop</td>
													<td>R$ {{ $event->price }}</td>
												</tr>
												<tr>
													<td>2 ou mais wkp tem-se desconto de 5%</td>
													<td></td>
												</tr>
												<tr>
													<td>Associados ABPModa têm desconto de 5%</td>
													<td></td>
												</tr>
												<tr>
													<td>Estudantes têm desconto de 5%</td>
													<td></td>
												</tr>
												<tr>
													<td>Instituições conveniadas têm desconto de até 5%</td>
													<td></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div> <!-- /panel -->

							@if($related_events->count() >0)
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingTwo">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" class="collapsed">
											Eventos relacionados
										</a>
									</h4>
								</div>
								
								<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
									<div class="panel-body">
										
										@foreach($related_events as $related)
											<p>
												<a href="{{ route('events.show',$related->slug) }}">
												<span>{{ $related->city->name or null }} 
												@if(!empty($related->city->state->uf )) 
												- 
												@endif 
												{{ $related->city->state->uf or null }}</span> 
												@if(!empty($related->city->name ) and !empty($related->city->state->uf)) 
												| 
												@endif 
												{{ $related->name }}
											</a></p>
										@endforeach
									</div>
								</div>
					
							</div> <!-- /panel -->
							@endif
						</div> <!-- /panel-group -->

						<div class="col-md-12">
							<h2>Facilitado por:</h2>
							<div class="col-md-2">
								{{-- HTML::image('uploads/') --}}
								<img src="images/frentes-de-atuacao/avatar-facilitador.jpg" alt="">
							</div>	
							<div class="col-md-10">
								<h3>{{ $event->facilitator->name  }}</h3>
								<p>{{ $event->facilitator->content }}</p>
							</div>
						</div>
						@if($facilitator_events->count() > 0)
						<div class="col-md-12">
							<div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">

								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingTree">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion2" href="#collapseTree" aria-expanded="true" aria-controls="collapseTree" class="collapsed">
												Outros eventos deste facilitador
											</a>
										</h4>
									</div>
									<div id="collapseTree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTree">
										<div class="panel-body">
											@foreach($facilitator_events as $f_event)
												@if($f_event->id != $event->id)
													<a href="{{ route('events.show',$f_event->slug) }}">
														<p><span>{{ $f_event->city->name or null }} 
														@if(!empty($f_event->city->state->uf )) 
														- 
														@endif 
														{{ $f_event->city->state->uf or null }}</span> 
														@if(!empty($f_event->city->name ) and !empty($f_event->city->state->uf)) 
														| 
														@endif 
														{{ $f_event->name }}</p>
															</a>
														@endif
											@endforeach
										</div>
									</div>
								</div> <!-- /panel -->

							</div> <!-- /panel-group -->
						</div> <!-- /col -->
						@endif
					</div> <!-- /col -->
			    </div> <!-- /workshops -->

			    <div id="talkshows" class="tab-pane fade">
			    	<div class="col-md-12">
						<h3>Menu 1</h3>
						<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
					</div>
			    </div> <!-- /talkshows -->

			    <div id="programas_especiais" class="tab-pane fade">
			    	<div class="col-md-12">
						<h3>Menu 2</h3>
						<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
					</div>
			    </div> <!-- /programas_especiais -->

			    <div id="todos_os_meses" class="tab-pane fade">
			    	<div class="col-md-12">
						<p>
							<a href="#"><span>12/05/2015 - </span>Direção de Arte para Campanhas de Moda - Eduardo Varela</a>
						</p>

						<p>
							<a href="#"><span>17/05/2015 - </span>Direção de Arte para Campanhas de Moda - Eduardo Varela</a>
						</p>

						<p>
							<a href="#"><span>03/06/2015 - </span>Direção de Arte para Campanhas de Moda - Eduardo Varela</a>
						</p>

						<p>
				      		<a href="#"><span>03/06/2015 - </span>Direção de Arte para Campanhas de Moda - Eduardo Varela</a>
						</p>

						<p>
							<a href="#"><span>03/06/2015 - </span>Direção de Arte para Campanhas de Moda - Eduardo Varela</a>
						</p>

						<p>
							<a href="#"><span>03/06/2015 - </span>Direção de Arte para Campanhas de Moda - Eduardo Varela</a>
						</p>

						<p>
							<a href="#"><span>03/06/2015 - </span>Direção de Arte para Campanhas de Moda - Eduardo Varela</a>
						</p>

						<p>
							<a href="#"><span>03/06/2015 - </span>Direção de Arte para Campanhas de Moda - Eduardo Varela</a>
						</p>

						<p>
							<a href="#"><span>03/06/2015 - </span>Direção de Arte para Campanhas de Moda - Eduardo Varela</a>
						</p>

						<p>
							<a href="#"><span>03/06/2015 - </span>Direção de Arte para Campanhas de Moda - Eduardo Varela</a>
						</p>

						<p>
							<a href="#"><span>03/06/2015 - </span>Direção de Arte para Campanhas de Moda - Eduardo Varela</a>
						</p>

						<p>
							<a href="#"><span>03/06/2015 - </span>Direção de Arte para Campanhas de Moda - Eduardo Varela</a>
						</p>

						<p>
							<a href="#"><span>03/06/2015 - </span>Direção de Arte para Campanhas de Moda - Eduardo Varela</a>
						</p>

						<p>
							<a href="#"><span>03/06/2015 - </span>Direção de Arte para Campanhas de Moda - Eduardo Varela</a>
						</p>

						<p>
							<a href="#"><span>03/06/2015 - </span>Direção de Arte para Campanhas de Moda - Eduardo Varela</a>
						</p>
					</div>
			    </div> <!-- /todos_os_meses -->

			    <div class="col-md-12">
				</div> <!-- /col -->

			</div> <!-- /tab-content -->
		</div>
	</div> <!-- /row -->

	<div class="row">
		
	</div> <!-- /row -->

</div> <!-- /page -->
@stop