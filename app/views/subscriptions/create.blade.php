@section('content')
<div id="page-sacola">

	<div class="row">
		<div class="col-md-12">
			<div class="titulo">
				<div class="txt">
					<h1>Minha sacola</h1>
				</div>
			</div>
		</div>
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-12">
			<h2>Eventos</h2>
			<table class="table">
				<tbody>
					@forelse($events as $event)
					
					<tr>
						<td>{{ $event->city->name or null }} - {{ $event->title->name or null }} {{ $event->name }} - {{ $event->facilitator->name or null }}</td>
						<td>
							{{ Form::open(['route'=>array('cart.delete.item',$event->id)]) }}
								<button style="border:none;background:none;" type="submit"><i style="color:#3D3D3D;font-size:20px; padding-top:10px;" class="fa fa-trash"></i></button>
							{{ Form::close() }}
						</td>
						<td>
							{{ Form::open(['route'=>array('cart.update.item',$event->id)]) }}
							<select name="qtd" class="bt-select bt_update_item">
								<option value="1">X1</option>
								<option value="2">X2</option>
								<option value="3">X3</option>
								<option value="4">X4</option>
								<option value="5">X5</option>
							</select>
							{{ Form::close() }}
						</td>
						
						<td>R$ {{ $event->price }}</td>
					</tr>
					
					@empty
					<tr>
						<tdcolspan="4">Vazio</td>
					</tr>
					@endforelse
					<tr>
						<td>Valor total</td>
						<td></td>
						<td></td>
						<td>R$ 3.249,00</td>
					</tr>
					<tr>
						<td>Desconto aplicado</td>
						<td></td>
						<td></td>
						<td>10%</td>
					</tr>
					<tr class="valor-final">
						<td>Valor final</td>
						<td></td>
						<td></td>
						<td>R$ 2.924,00</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-8">
			<h2>Formas de pagamento</h2>
			<p>Selecione a forma de pagamento.</p>
			
			<form role="form">
				<div class="box-borda">
				
					<div class="form-inline form-group">
				    	<label class="radio-inline">
							<input type="radio" name="pagamento" id="pagseguro" value="pagseguro"> <img src="images/logo-pagseguro.png" alt="">
						</label>
					</div>
					<p>Escolhemos a empresa Pag Seguro/UOL para efetuar nossas transações online com o objetivo de oferecer a você mais conforto, segurança e opções de pagamento.</p>
					<img src="images/opcoes-pagamentos.jpg" alt="" class="img-responsive">
				</div> <!-- /box-borda -->	
		
				<div class="box-borda">
					<div class="form-inline form-group">
				    	<label class="radio-inline">
							<input type="radio" name="pagamento" id="deposito" value="deposito"> Depósito em conta
						</label>
					</div>
					<p>Banco: Nome do Banco</p>
					<p>
						Agência: 9999-9 <br/>
						Conta: 999999-9
					</p>
				
				</div> <!-- /box-borda -->	
			</form>
		</div>
	</div> <!-- /row -->

	<div class="row">
		<div class="col-md-12">
			<hr>
			<button type="submit" class="bt-padrao right">Finalizar compra <i class="icone-seta-direita"></i></button>
			<button type="submit" class="bt-padrao right">Continuar comprando <i class="icone-seta-direita"></i></button>			
		</div>
	</div> <!-- /row -->

</div> <!-- /page -->
@stop