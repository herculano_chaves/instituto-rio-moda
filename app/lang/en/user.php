<?php 

return array(

	'docError' => ':element inválido',
	'notfound' => 'Usuário não encontrado',
	'activated' => 'Usuário ativado com sucesso, agora já pode se logar e participar de todos nossos eventos e promoções',
	'notactivated' => 'Erro ao ativar usuário',

	'register' => array(
		'success' => 'Usuário registrado com sucesso, agora acesse seu Email com as instruções para sua validação',
		'error' => 'Erro ao cadastrar usuário, tente novamente mais tarde',
		'exists' => 'Usuário já existente!',
		'required' => array(
			'login' => 'Email obrigatório',
			'pass' => 'Senha obrigatória'
		)
	)
);