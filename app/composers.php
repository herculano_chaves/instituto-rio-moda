<?php 
View::composer('layouts.partials.sidebar', function($view) {
   
   $taxonomies = Taxonomy::all();

   $view->with('taxonomies', $taxonomies);
});