<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserdatasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('userdatas', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('covenant_id');
			$table->string('tel1');
			$table->string('tel2');
			$table->string('birthday');
			$table->string('rg');
			$table->string('registar');
			$table->string('site');
			$table->string('scolarity');
			$table->string('pool');	
			$table->integer('student');	
			$table->string('course');	
			$table->string('institution');	
			$table->string('year_graduation');	
			$table->string('num_course_register');	
			
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('userdatas');
	}

}
