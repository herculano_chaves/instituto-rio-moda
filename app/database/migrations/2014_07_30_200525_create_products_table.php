<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('facilitator_id');
			$table->integer('city_id');
			$table->integer('state_id');
			$table->string('address');
			$table->integer('featured_level');
			$table->string('name');
			$table->string('duration');	
			$table->date('date_ini');
			$table->date('date_fin');
			$table->string('hour');
			$table->text('content');
		
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products');
	}

}
