<?php

class Userdata extends \Eloquent {
	protected $fillable = ['education_id', 'institution_id', 'tel', 'cel', 'birthday', 'cpf', 'cnpj', 'rg', 'registar', 'site', 'company', 'Area', 'address', 'number', 'complement', 'district', 
'city_id','state_id','cep', 'course', 'institution', 'year_graduation', 'num_course_register', 'poll'];

	public static $rules = [
		'name'       => 'required', 
		'estudante' =>'required',
		'tel'       => 'required', 
		'cel'       => 'required', 
		'birthday'  => 'required', 
		'cpf'       => 'required_without:cnpj', 
		'cnpj'      => 'required_without:cpf', 
		'rg'        => 'required', 
		'registar'   => 'required', 
		'address'    => 'required', 
		'number'     => 'required', 
		'district'   => 'required', 
		'city_id'    => 'required',
		'state_id'   => 'required',
		'cep'        => 'required', 
		'poll'       => 'required', 
		'education_id'  => 'required', 
	];

	public $timestamps = false;

	public function user(){
		return $this->belongsTo('User');
	}

	public function city(){
		return $this->belongsTo('City');
	}
	
}