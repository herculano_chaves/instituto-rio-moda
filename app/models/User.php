<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	public static $rules = [
		'email' => 'required',
		'password' => 'required|min:4|confirmed',
		
	];

	public function userdata(){
		return $this->hasOne('Userdata');
	}

	public function scopeExceptAdmin($query){
		return $query->where('permissions','=','');
	}

	public function notices(){
		return $this->hasMany('Notice');
	}
}
