<?php

class City extends \Eloquent {
	protected $fillable = [];

	public function state(){
		return $this->belongsTo('State');
	}

	public function userdatas(){
		return $this->hasMany('Userdata');
	}

	public function products(){
		return $this->hasMany('Product');
	}
}