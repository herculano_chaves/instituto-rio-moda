<?php

class Institution extends \Eloquent {
	protected $fillable = ['name', 'active', 'discount'];

	public static $rules = [
		'name' => 'required|unique:institutions',
		'discount' => 'required'
	];
}