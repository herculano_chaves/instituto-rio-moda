<?php

class Taxonomy extends \Eloquent {
	protected $fillable = ['name'];

	public static $rules = ['name'=>'required'];

	public function categories(){
		return $this->hasMany('Category');
	}

	public function products(){
		return $this->hasManyThrough('Product','Category', 'taxonomy_id', 'category_id');
	}
}