<?php

class Notice extends \Eloquent {
	protected $fillable = ['title','lead','content'];

	public static $rules = [
		'title' => 'required',
		'lead' => 'required',
		'content' => 'required'
	];

	public function user(){
		return $this->belongsTo('User');
	}
}