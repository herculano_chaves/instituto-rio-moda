<?php

class Category extends \Eloquent {
	protected $fillable = ['parent', 'name', 'taxonomy_id'];

	public static $rules = [
		'name'=>'required',
		'taxonomy_id'=>'required'
	];

	public function products(){
		return $this->hasMany('Product');
	}

	public function taxonomy(){
		return $this->belongsTo('Taxonomy');
	}

	public function parentName(){

		$c = Category::find($this->parent);
		
		return isset($c->name) ? $c->name : null;
	}
}