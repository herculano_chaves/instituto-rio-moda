<?php

class Banner extends \Eloquent {
	protected $fillable = ['name',	'featured', 'link', 'src', 'active'];

	public static $rules = ['name'=>'required',	'featured'=>'required',  'src'=>'required'];

	public function scopeFeatured($query, $featured_level){
		return $query->where('featured', '=', $featured_level);
	}

	public function scopeOnlyActive($query){
		return $query->whereActive('1');
	}
}