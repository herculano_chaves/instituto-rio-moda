<?php

class Product extends \Eloquent {
	protected $fillable = [
		'slug','address', 'number', 'complement', 'district', 'featured', 'name', 'duration', 'date_ini', 'date_fin', 'content', 'price', 
		'hour','curriculum', 'facilitator_id','category_id','info_discount','city_id','related_events','active','date_event_register_limit'
	];

	protected $table = 'events';

	public static $rules = [
		'address'        => 'required',
		'title_id'       => 'required',
		'duration'       => 'required',
		'date_ini'       => 'required|date_format:"d/m/Y"',
		'date_fin'       => 'required|date_format:"d/m/Y"',
		'content'        => 'required',
		'price'          => 'required',
		'facilitator_id' => 'required',
		'category_id' => 'required',
		'city_id'        => 'required',
		'number'         => 'required',
		'district'       =>  'required',
		'hour'           =>  'required'
	];

	public function facilitator(){
		return $this->belongsTo('Facilitator');
	}

	public function title(){
		return $this->belongsTo('Title');
	}

	public function category(){
		return $this->belongsTo('Category');
	}

	public function city(){
		return $this->belongsTo('City');
	}

	public function taxonomy(){
		return $this->belongsTo('Category');
	}

	public function scopeTax($query, $tax){
		return $query->join('categories', 'events.category_id', '=', 'categories.id')
		->join('taxonomies', 'categories.taxonomy_id', '=', 'taxonomies.id')
		->where('taxonomies.slug',$tax)
		->select('events.*');
	}

	public function cat($cat){
		return Category::whereCategory_id($cat);
	}


	public function scopeFeatured($query, $featured_level){
		return $query->where('featured', '=', $featured_level);
	}

	public function scopeOnlyActive($query){
		return $query->whereActive('1');
	}

	public function scopeActualDate($query){
		return $query->where('date_ini','<', date('Y-m-d h:i:s'))
				->where('date_fin', '>', date('Y-m-d h:i:s'));
	}

	public function scopeNextDate($query){
		return $query->where('date_fin','>', date('Y-m-d h:i:s'));
		
	}
}