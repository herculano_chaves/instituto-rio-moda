<?php

class Facilitator extends \Eloquent {
	protected $fillable = ['name','content'];

	public static $rules = [
		'name'=>'required',
		'content'=>'required',
	];
	
	public function events(){
		return $this->hasMany('Product');
	}
}