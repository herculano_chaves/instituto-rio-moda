<?php

class State extends \Eloquent {
	protected $fillable = [];

	public function cities(){
		return $this->hasMany('City');
	}
}