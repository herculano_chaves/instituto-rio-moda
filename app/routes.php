<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', ['as'=>'home', 'uses'=>'SiteController@index']);
Route::get('quem-somos', ['as'=>'quemSomos', 'uses'=>'SiteController@quemSomos']);

# Forms
Route::get('contato', ['as'=>'contato', 'uses'=>'SiteController@contato']);
Route::post('contato', ['as'=>'contato.post', 'uses'=>'SiteController@postContato']);

# Passwords
Route::get('password/reset', ['uses' => 'PasswordController@remind',  'as' => 'password.remind']);
Route::post('password/reset', ['uses' => 'PasswordController@request', 'as' => 'password.request']);
Route::get('password/reset/{token}', ['uses' => 'PasswordController@reset', 'as' => 'password.reset']);
Route::post('password/reset/{token}', ['uses' => 'PasswordController@update', 'as' => 'password.update']);



//Route::get('portfolio-de-atividades', ['as'=>'atividades', 'uses'=>'SiteController@atividades']);
Route::get('descontos', ['as'=>'descontos', 'uses'=>'SiteController@descontos']);
//Route::get('minha-sacola', ['as'=>'minhaSacola', 'uses'=>'SiteController@minhaSacola']);
Route::get('recuperar-senha', ['as'=>'recuperarSenha', 'uses'=>'SiteController@recuperarSenha']);
Route::get('resultado-busca', ['as'=>'resultadoBusca', 'uses'=>'SiteController@resultadoBusca']);

# Events
Route::get('evento/{slug}', ['as'=>'events.show', 'uses'=>'ProductsController@show']);
Route::get('produtos/{tax}/{category?}', ['as'=>'events.index', 'uses'=>'ProductsController@results']);

# Users/customers
Route::get('cadastro', ['as'=>'user.register', 'uses'=>'UsersController@register']);
Route::post('cadastro', ['as'=>'user.register.post', 'uses'=>'UsersController@postRegister']);
Route::get('cadastro/resultado', ['as'=>'user.register.return', 'uses'=>'UsersController@result']);
Route::get('cadastro/confirmacao/{activationCode}', ['as'=>'user.register.activation', 'uses'=>'UsersController@activation']);

Route::post('login',['as'=>'user.login', 'uses'=>'UsersController@login']);
Route::get('logout',['as'=>'user.logout', 'uses'=>'UsersController@logout']);

# User account area
Route::group(['before'=>'auth'], function(){
	Route::get('minha-conta',['as'=>'user.edit', 'uses'=>'UsersController@edit']);
	Route::post('minha-conta',['as'=>'user.update', 'uses'=>'UsersController@update']);

	# Meus eventos
	Route::get('eventos', ['as'=>'user.events', 'uses'=>'UsersController@events']);

	# Reset senha
	Route::post('password/reset', ['uses' => 'PasswordController@userUpdate', 'as' => 'user.password.update']);

	# Compras
	Route::post('adiciona/{event_id}', ['as'=>'event.add', 'uses'=>'SubscriptionsController@addCart']);
	Route::get('minha-sacola', ['as'=>'subscription.create', 'uses'=>'SubscriptionsController@create']);
	Route::post('retira/item/{event_id}', ['as'=>'cart.delete.item', 'uses'=>'SubscriptionsController@deleteItem']);
	Route::post('atualiza/item/{event_id}', ['as'=>'cart.update.item', 'uses'=>'SubscriptionsController@updateItem']);
});

# Imprensa
Route::get('imprensa', ['as'=>'imprensa.index', 'uses'=>'NoticesController@siteIndex']);
Route::get('imprensa/{id}', ['as'=>'imprensa.show', 'uses'=>'NoticesController@show']);

# Admin
Route::get('admin', ['as'=>'admin.index', 'before'=>'already.log.admin', 'uses'=>'AdminController@index']);
Route::post('admin', ['as'=>'admin.login', 'uses'=>'AdminController@login']);
Route::get('admin/logout', ['as'=>'admin.logout', 'uses'=>'AdminController@logout']);

Route::group(['prefix'=>'admin', 'before'=>'auth|auth.admin'], function(){
	
	# Eventos
	Route::resource('eventos', 'ProductsController');
	
	# Facilitadores
	Route::resource('facilitator', 'FacilitatorsController');

	# Áreas
	Route::resource('areas', 'CategoriesController');

	# Taxonomias
	Route::resource('taxonomias', 'TaxonomiesController');

	# Clientes
	Route::resource('clientes', 'UsersController');

	# Instituições
	Route::resource('instituicoes', 'InstitutionsController');

	# Imprensa
	Route::resource('imprensa', 'NoticesController');

	# Titulos
	Route::resource('titulos', 'TitlesController');

	# Newsletters
	Route::resource('newsletters', 'NewslettersController');

	# Banners
	Route::resource('banners', 'BannersController');
	
	# Apoiadores
	Route::resource('apoiadores', 'ApoiadoresController');
});

# Searches
Route::post('city/search', ['as'=>'city.search', 'uses'=>'CitiesController@searchByState']);

Route::get('meus-dados', ['as'=>'meusDados', 'uses'=>'SiteController@meusDados']);
Route::get('meus-pedidos', ['as'=>'meusPedidos', 'uses'=>'SiteController@meusPedidos']);



