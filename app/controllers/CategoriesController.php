<?php

class CategoriesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /categories
	 *
	 * @return Response
	 */
	protected $layout = 'layouts.base';

	public function index()
	{
		//
		$areas = Category::orderBy('id','DESC')->paginate(20);

		return View::make('admin.categories.index', compact('areas'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /categories/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		$areas = Category::orderBy('id','DESC')->lists('name','id');

		$taxonomies = Taxonomy::orderBy('id','DESC')->lists('name','id');

		return View::make('admin.categories.create', compact('areas','taxonomies'));
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /categories
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		$data = Input::all();

		$validator = Validator::make($data, Category::$rules);

		if($validator->fails()){
			return Redirect::route('admin.areas.create')->withErrors($validator)->withInput();
		}

		$category = Category::create($data);

		$category->slug = Slugify::slugify($data['name']);
		$category->save();

		return Redirect::route('admin.areas.index')->withSuccess(Lang::get('crud.create.success', ['element'=>'Área']));
	}

	/**
	 * Display the specified resource.
	 * GET /categories/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /categories/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$area = Category::find($id);

		$areas = Category::orderBy('id','DESC')->lists('name','id');

		$taxonomies = Taxonomy::orderBy('id','DESC')->lists('name','id');

		return View::make('admin.categories.edit', compact('area','areas','taxonomies'));
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /categories/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$area = Category::find($id);

		if($area->name != Input::get('name')){
			$area->slug = Slugify::slugify(Input::get('name'));
			$area->save();
		}

		$area->update(Input::all());

		return Redirect::route('admin.areas.edit',['id'=>$id])->withSuccess(Lang::get('crud.update.success', ['element'=>'Área']));

	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /categories/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$area = Category::find($id);

		$area->delete();

		return Redirect::route('admin.areas.index')->withSuccess(Lang::get('crud.destroy.success', ['element'=>'Área']));

	}

}