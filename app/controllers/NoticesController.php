<?php

class NoticesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /notices
	 *
	 * @return Response
	 */
	protected $layout = 'layouts.base';

	public function index()
	{
		//
		$notices = Notice::orderBy('id','DESC')->paginate(20);

		return View::make('admin.notices.index', compact('notices'));
	}

	public function siteIndex()
	{
		//
		$notices = Notice::orderBy('id','DESC')->paginate(20);

		$this->layout->content = View::make('notices.index', compact('notices'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /facilitators/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		return View::make('admin.notices.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /notices
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		$data = Input::all();

		$validator = Validator::make($data, Notice::$rules);

		if($validator->fails()){
			return Redirect::route('admin.imprensa.create')->withErrors($validator)->withInput();
		}

		$notice = Notice::create($data);

	
		$allowedExts = array("gif", "jpeg", "jpg", "png");
		$arr = explode(".", $_FILES["src"]["name"]);
		$extension = end($arr);
			if ((($_FILES["src"]["type"] == "image/gif")
			|| ($_FILES["src"]["type"] == "image/jpeg")
			|| ($_FILES["src"]["type"] == "image/jpg")
			|| ($_FILES["src"]["type"] == "image/png"))
			&& ($_FILES['src']['error'] == '0')
			&& in_array($extension, $allowedExts)){
				
				$image = $this->upload_file($_FILES['src'], 372, 242, 'uploads/imprensa');
				$thumb = $this->upload_file($_FILES['src'], 230, 172, 'uploads/imprensa/thumb', $image->file_dst_name_body);
				$notice->src =  $image->file_dst_name;
				$notice->save();
			}

		return Redirect::route('admin.imprensa.index')->withSuccess(Lang::get('crud.create.success', ['element'=>'Imprensa']));
	}

	/**
	 * Display the specified resource.
	 * GET /facilitators/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$notice = Notice::find($id);

		$this->layout->content = View::make('notices.show', compact('notice'));
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /facilitators/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$notice = Notice::find($id);

		return View::make('admin.notices.edit', compact('notice'));
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /facilitators/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
		$notice = Notice::find($id);

		$notice->update(Input::all());

		$allowedExts = array("gif", "jpeg", "jpg", "png");
		$arr = explode(".", $_FILES["src"]["name"]);
		$extension = end($arr);
			if ((($_FILES["src"]["type"] == "image/gif")
			|| ($_FILES["src"]["type"] == "image/jpeg")
			|| ($_FILES["src"]["type"] == "image/jpg")
			|| ($_FILES["src"]["type"] == "image/png"))
			&& ($_FILES['src']['error'] == '0')
			&& in_array($extension, $allowedExts)){
				if(isset($notice->src)):
					File::delete('uploads/imprensa/'.$notice->src);
					File::delete('uploads/imprensa/thumb/'.$notice->src);
				endif;
				$image = $this->upload_file($_FILES['src'], 372, 242, 'uploads/imprensa');
				$thumb = $this->upload_file($_FILES['src'], 230, 172, 'uploads/imprensa/thumb', $image->file_dst_name_body);
				$notice->src =  $image->file_dst_name;
				$notice->save();
			}


		return Redirect::route('admin.imprensa.edit',['id'=>$id])->withSuccess(Lang::get('crud.update.success', ['element'=>'Imprensa']));

	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /facilitators/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($notice)
	{
		//
		$notice = Notice::find($notice);

		if(isset($notice->src)):
			File::delete('uploads/imprensa/'.$notice->src);
			File::delete('uploads/imprensa/thumb/'.$notice->src);
		endif;

		$notice->delete();

		return Redirect::route('admin.imprensa.index')->withSuccess(Lang::get('crud.destroy.success', ['element'=>'Imprensa']));
	}

}