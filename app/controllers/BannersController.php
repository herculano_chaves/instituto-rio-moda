<?php

class BannersController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /banners
	 *
	 * @return Response
	 */
	protected $layout = 'layouts.base';

	public function index()
	{
		//
		$banners = Banner::orderBy('id','DESC')->paginate(20);

		return View::make('admin.banners.index', compact('banners'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /banners/create
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('admin.banners.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /banners
	 *
	 * @return Response
	 */
	public function store()
	{
		$data = Input::all();

		$validator = Validator::make($data, Banner::$rules);

		if($validator->fails()){
			return Redirect::route('admin.banners.create')->withErrors($validator)->withInput();
		}

		$banner = Banner::create($data);

	
		$allowedExts = array("gif", "jpeg", "jpg", "png");
		$arr = explode(".", $_FILES["src"]["name"]);
		$extension = end($arr);
			if ((($_FILES["src"]["type"] == "image/gif")
			|| ($_FILES["src"]["type"] == "image/jpeg")
			|| ($_FILES["src"]["type"] == "image/jpg")
			|| ($_FILES["src"]["type"] == "image/png"))
			&& ($_FILES['src']['error'] == '0')
			&& in_array($extension, $allowedExts)){
				
				$image = $this->upload_file($_FILES['src'],782,90,'uploads/banners');
				$banner->src =  $image->file_dst_name;
				$banner->save();
			}

		
		

		return Redirect::route('admin.banners.index')->withSuccess(Lang::get('crud.create.success', ['element'=>'Banner']));
	}

	/**
	 * Display the specified resource.
	 * GET /banners/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /banners/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$banner = Banner::find($id);
		
		return View::make('admin.banners.edit', compact('banner'));
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /banners/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /banners/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}