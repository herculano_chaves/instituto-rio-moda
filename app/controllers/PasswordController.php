<?php
class PasswordController extends BaseController {
 
	public function remind()
	{
	  return View::make('password.remind');
	}

  	public function request()
	{
	 	try
		{
		    $user = Sentry::findUserByLogin(Input::get('email'));

		    $resetCode = $user->getResetPasswordCode();

		    $data = ['user'=>$user, 'resetCode'=>$resetCode];

		    Mail::send('emails.auth.reminder', $data, function($message)
			{
			  $message->to(Input::get('email'), 'Instituto Rio Moda nova senha')
			          ->subject('Instituto Rio Moda nova senha');
			});

			return Redirect::route('password.remind')->with('success','Foi enviado para o seu email instruções para criação de uma nova senha!');
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		   return Redirect::back()->with('error','Usuário não encontrado!');
		}
	}

	public function reset($token)
	{
		try{
			$user = Sentry::findUserByResetPasswordCode($token);
			 return View::make('password.reset')->with('token', $token);
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e){
			return Redirect::to('/')->with('error','Token já utilizado');
		}
	 
	}
	public function update()
	{
		$data =Input::all();
		
		$validate = Validator::make($data, ['password'=>'required|min:4','password_confirmation'=>'required|same:password'], ['password.required'=>'Favor preencher senha','password.min'=>'Senha deve conter mais de 4 caracteres','password_confirmation.required'=>'Favor preencher confirmação de senha','password_confirmation.same'=>'Sua confirmação deve ser igual ao campo senha']);

		if($validate->fails()){
			return Redirect::back()->withInput()->withErrors($validate);
		}else{
			$token = Input::get('token');
			 try
			{
			    // Find the user using the user id
			   $user = Sentry::findUserByResetPasswordCode($token);

			    // Check if the reset password code is valid
			    if ($user->checkResetPasswordCode($token))
			    {
			        // Attempt to reset the user password
			        if ($user->attemptResetPassword($token, Input::get('password')))
			        {
			            // Password reset passed
			            return Redirect::to('/')->with('success','Senha alterada com sucesso, favor logar com nova senha!');
			        }
			        else
			        {
			            // Password reset failed
			            return Redirect::back()->with('error','Erro ao enviar nova senha');
			        }
			    }
			    else
			    {
			        // The provided password reset code is Invalid
			        return Redirect::back()->with('error','Código ou url incorreto, favor verificar seu E-mail com o link correto, para fazer nova senha.');
			    }
			}
			catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
			{
			    return Redirect::to('/')->with('error','Usuário não encontrado');
			}
		}
	}

	public function userUpdate(){
		$data =Input::all();
		
		$validate = Validator::make($data, ['password'=>'required|min:4','password_confirmation'=>'required|same:password'], ['password.required'=>'Favor preencher senha','password.min'=>'Senha deve conter mais de 4 caracteres','password_confirmation.required'=>'Favor preencher confirmação de senha','password_confirmation.same'=>'Sua confirmação deve ser igual ao campo senha']);

		if($validate->fails()){
			return Redirect::back()->withInput()->withErrors($validate);
		}

		$user = $this->getUser();

		$user->password = Hash::make($data['password']);
		$user->save();

		return Redirect::route('user.edit')->withSuccess('Senha alterada com sucesso.');
	}
}