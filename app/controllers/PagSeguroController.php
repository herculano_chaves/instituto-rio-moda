<?php 
//use source\PagseguroLibrary\PagSeguroLibrary;

require_once "../vendor/pagseguro/php/source/PagSeguroLibrary/PagSeguroLibrary.php";

class PagSeguroController extends BaseController
{

	public function postPayment(){

		$course = Course::find(Input::get('course_id'));

		$user = Sentry::getUser();

        $address = Address::where('user_id','=',$user->id)
        ->whereActivated('1')
        ->get();
	  /*     echo "<pre>";
	   print_r($address);
	   echo "</pre>";
       echo $address[0]->zip;*/

        // Instantiate a new payment request
        $paymentRequest = new PagSeguroPaymentRequest();

        // Set the currency
        $paymentRequest->setCurrency("BRL");

        // Add an item for this payment request
        $paymentRequest->addItem('0001', $course->name, 1, number_format($course->price, 2, '.', ''));

       
        // Set a reference code for this payment request. It is useful to identify this payment
        // in future notifications.
        $paymentRequest->setReference("REF".$course->id.'-'.$user->id);

        // Set shipping information for this payment request
        $sedexCode = PagSeguroShippingType::getCodeByType('SEDEX');
        $paymentRequest->setShippingType($sedexCode);
        $paymentRequest->setShippingAddress(
            $address[0]->zip,
            $address[0]->address,
            null,
            null,
            null,
            $address[0]->city,
            $address[0]->state,
            'BRA'
        );

        // Set your customer information.
        $paymentRequest->setSender(
            $user->first_name.' '.$user->last_name,
            $user->email
        );

        // Set the url used by PagSeguro to redirect user after checkout process ends
        $paymentRequest->setRedirectUrl(URL::route('payment.pagseguro.return'));

    /*    // Add checkout metadata information
        $paymentRequest->addMetadata('PASSENGER_CPF', '15600944276', 1);
        $paymentRequest->addMetadata('GAME_NAME', 'DOTA');
        $paymentRequest->addMetadata('PASSENGER_PASSPORT', '23456', 1);

        // Another way to set checkout parameters
        $paymentRequest->addParameter('notificationURL', 'http://www.lojamodelo.com.br/nas');
        $paymentRequest->addParameter('senderBornDate', '07/05/1981');
        $paymentRequest->addIndexedParameter('itemId', '0003', 3);
        $paymentRequest->addIndexedParameter('itemDescription', 'Notebook Preto', 3);
        $paymentRequest->addIndexedParameter('itemQuantity', '1', 3);
        $paymentRequest->addIndexedParameter('itemAmount', '200.00', 3);*/

        try {

            /*
             * #### Credentials #####
             * Replace the parameters below with your credentials (e-mail and token)
             * You can also get your credentials from a config file. See an example:
             * $credentials = PagSeguroConfig::getAccountCredentials();
            //  */
            
            $credentials = new PagSeguroAccountCredentials("sharonrolim@easyjuris.com",
               "AE1E5238F71A4806BB59D1D18C6DE141");            

            // Register this payment request in PagSeguro to obtain the payment URL to redirect your customer.
            $url = $paymentRequest->register($credentials);
            
            
            return Redirect::away($url);



        } catch (PagSeguroServiceException $e) {

            die($e->getMessage());
        }
    }

	public function getPaymentReturn(){

		 $token = 'AE1E5238F71A4806BB59D1D18C6DE141';
		 $email = 'sharonrolim@easyjuris.com';
		 $tid = Input::get('transaction_id');

       
       $credentials = new PagSeguroAccountCredentials($email, $token);  
       $transaction = PagSeguroTransactionSearchService::searchByCode(    
            $credentials,    
            $tid    
        ); 


       var_dump($transaction);
		/* return Redirect::route('payment.paypal.return')
                ->with('success', 'Payment success')
                ->withTransaction($transaction);*/
	
	}

 /*   public function notification($transaction_code){
        try{
              $credentials = new PagSeguroAccountCredentials("sharonrolim@easyjuris.com",
               "AE1E5238F71A4806BB59D1D18C6DE141");        

            // application authentication
            //$credentials = PagSeguroConfig::getApplicationCredentials();

            //$credentials->setAuthorizationCode("E231B2C9BCC8474DA2E260B6C8CF60D3");

            $transaction = PagSeguroTransactionSearchService::searchByCode($credentials, $transaction_code);
        

            $u = Sentry::getUser();

            $user = User::find($u->id);

            $status_text = $user->customer->getStatusText($transaction->getStatus()->getValue());

            return Response::json(['data'=>$status_text]);
    
        }
        catch (PagSeguroServiceException $e) {
           // die($e->getMessage());

            return false;
        }
    }*/

    public function notification()
    {

        $notificationCode = (isset($_POST['notificationCode']) && trim($_POST['notificationCode']) !== "" ?
            trim($_POST['notificationCode']) : null);
        
        $credentials = new PagSeguroAccountCredentials("sharonrolim@easyjuris.com",
               "AE1E5238F71A4806BB59D1D18C6DE141"); 

        try {
            $transaction = PagSeguroNotificationService::checkTransaction($credentials, $notificationCode);
            // Do something with $transaction

        } catch (PagSeguroServiceException $e) {
            die($e->getMessage());
        }
    }


}