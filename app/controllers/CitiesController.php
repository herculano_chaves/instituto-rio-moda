<?php

class CitiesController extends \BaseController {

	public function searchByState(){
		if(Request::ajax()){
			$cities = City::where('state_id',Input::get('state'))->get();


			$output = '<option value="">Selecione sua cidade</option>';

			foreach ($cities as $c) {
				$output .= '<option value="'.$c->id.'">'.$c->name.'</option>';
			}

			return Response::json(['response' => $output]);
		}
	}

}