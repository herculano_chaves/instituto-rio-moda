<?php

class UsersController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /users
	 *
	 * @return Response
	 */
	protected $layout = 'layouts.base';

	public function index()
	{
		$users = User::orderBy('id','DESC')->paginate(25);

		//print_r($users);

		return View::make('users.index', compact('users'));
	}

	public function register(){

		$institutions = Institution::orderBy('name')->lists('name','id');

		$educations = Education::orderBy('name')->lists('name','id');
		$states = State::orderBy('name','ASC')->lists('uf','id');

		$this->layout->content = View::make('users.register', compact('institutions','educations','states'));
	}

	public function postRegister(){
		$data = Input::all();

		$validator = Validator::make($data, Userdata::$rules);

		$user_validator = Validator::make($data, User::$rules);

		// Basic validatios about Userdata
		if($validator->fails()){
			return Redirect::route('user.register')->withErrors($validator)->withInput();
		}
		if($user_validator->fails()){
			return Redirect::route('user.register')->withErrors($user_validator)->withInput();
		}

		// Documental Validations
		$doc = isset($data['cpf']) ? $data['cpf'] : $data['cnpj'] ;
		$docvalid = new ValidaDoc($doc);

		if(!$docvalid->valida()){
			return Redirect::route('user.register')->withError(Lang::get('user.docError', ['element'=>$docvalid->verifica_cpf_cnpj()]));
		}

		try
		{
		    // Let's register a user.
		    $user = Sentry::register(array(
		        'email'    => $data['email'],
		        'password' => $data['password'],
		        'username' => $data['name']
		    ));
		   
		    $u = User::find($user->id);

		    $userdata = Userdata::create($data);

		    $userdate->birthday = Helper::ConverterUS($data['birthday']);
		    $userdata->user()->associate($u);
		    $userdata->save();

		    // Let's get the activation code
		    $activationCode = $user->getActivationCode();

		    // Send activation code to the user so he can activate the account
		    Mail::send('emails.users.register', array('activationCode'=>$activationCode, 'data'=>$data), function($message){

				$message->to(Input::get('email'),Input::get('name'))->subject('Confirmação de cadastro');

			});

			return Redirect::route('user.register.return')->withSuccess(Lang::get('user.register.success'));
		}
		catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
		{
		     return Redirect::route('user.register')->withSuccess(Lang::get('user.register.required.login'))->withInput();
		}
		catch (Cartalyst\Sentry\Users\PasswordrequireduiredException $e)
		{
		     return Redirect::route('user.register')->withSuccess(Lang::get('user.register.required.pass'))->withInput();
		}
		catch (Cartalyst\Sentry\Users\UserExistsException $e)
		{
		     return Redirect::route('user.register')->withSuccess(Lang::get('user.register.exists'))->withInput();
		}
	}

	public function result(){
		$this->layout->content = View::make('users.register-result');
	}

	public function activation($activationCode){

		 try
		{
		     $user = Sentry::findUserByActivationCode($activationCode);

		    if ($user->attemptActivation($activationCode))
		    {
		        // User activation passed
		        return Redirect::route('user.register')->withSuccess(Lang::get('user.activated'));

		    }
		    else
		    {
		        // User activation failed
		         return Redirect::route('user.register')->withError(Lang::get('user.notactivated'));
		    }
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		   return Redirect::route('user.register')->withError(Lang::get('user.notfound'));
		}
	}

	public function login(){
		return $this->masterLogin('user.register','user.edit');
	}

	public function logout(){
		return $this->masterLogout('home');
	}

	public function events(){
		$user = $this->getUser();

		$this->layout->content = View::make('users.events', compact('user'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /users/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /users
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /users/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /users/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit()
	{
		$u = Sentry::getUser();

		$institutions = Institution::orderBy('name')->lists('name','id');

		$educations = Education::orderBy('name')->lists('name','id');
		$states = State::orderBy('name','ASC')->lists('uf','id');

		$user = User::find($u->id);

		$cities = City::whereState_id($user->userdata->state_id)->lists('name','id');

		$this->layout->content = View::make('users.edit', compact('user','institutions','educations','states','cities'));
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /users/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /users/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}