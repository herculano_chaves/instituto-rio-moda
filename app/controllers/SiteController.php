<?php

class SiteController extends BaseController {

	protected $layout = 'layouts.base';

	public function index()
	{

		$products_featured_1 = Product::onlyActive()->nextDate()->featured(4)->orderBy('id', 'DESC')->take(5)->get();
		$products_featured_2 = Product::onlyActive()->nextDate()->featured(2)->orderBy('id', 'DESC')->take(2)->get();
		$products_featured_3 = Product::onlyActive()->nextDate()->featured(3)->orderBy('id', 'DESC')->take(1)->get();

		//Banners
		$banner_main = Banner::featured(1)->onlyActive()->take(1)->first();
		$banner_1 = Banner::featured(2)->onlyActive()->take(1)->first();
		$banner_2 = Banner::featured(3)->onlyActive()->take(1)->first();

	 	$this->layout->content = View::make('site.home', compact('products_featured_2', 'products_featured_3','banner_main','banner_1','banner_2'))
	 							->nest('slider_home_featured', 'layouts.partials.home.slider', compact('products_featured_1'));		

	}

	public function quemSomos()
	{
	 	$this->layout->content = View::make('site.quem-somos');
	}

	public function atividades()
	{
	 	$this->layout->content = View::make('site.portfolio-de-atividades');
	}

	public function descontos()
	{
	 	$this->layout->content = View::make('site.descontos');
	}

	public function contato()
	{
	 	$this->layout->content = View::make('site.contato');
	}
	public function postContato()
	{
	 	$data = Input::all();

	 	$validator = Validator::make($data, ['name'=>'required','email'=>'required', 'subject'=>'required','message'=>'required']);

	 	if($validator->fails()){
	 		return Redirect::route('contato')->withErrors($validator)->withInput();
	 	}

	 	Mail::send('emails.site.contact', ['data'=>$data], function($message){
	 		$message->to('herculano@dizain.com.br', 'IRM')->subject('Contato Instituto Rio Moda');
	 	});

	 	return Redirect::route('contato')->withSuccess('Contato recebido com sucesso, aguarde em breve entraremos em contato.');
	}

	public function imprensa()
	{
	 	$this->layout->content = View::make('site.imprensa');
	}

	public function imprensaInterna()
	{
	 	$this->layout->content = View::make('site.imprensa-interna');
	}

	public function cadastro()
	{
	 	$this->layout->content = View::make('site.cadastro');
	}

	public function minhaSacola()
	{
	 	$this->layout->content = View::make('site.minha-sacola');
	}

	public function recuperarSenha()
	{
	 	$this->layout->content = View::make('site.recuperar-senha');
	}

	public function resultadoBusca()
	{
	 	$this->layout->content = View::make('site.resultado-busca');
	}

	public function meusDados()
	{
	 	$this->layout->content = View::make('site.meus-dados');
	}

	public function meusPedidos()
	{
	 	$this->layout->content = View::make('site.meus-pedidos');
	}

}
