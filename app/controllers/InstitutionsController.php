<?php

class InstitutionsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /institutions
	 *
	 * @return Response
	 */
	protected $layout = 'layouts.base';

	public function index()
	{
		$institutions = Institution::orderBy('id', 'DESC')->paginate(25);

		return View::make('admin.institutions.index', compact('institutions'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /institutions/create
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('admin.institutions.create', compact('institutions'));
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /institutions
	 *
	 * @return Response
	 */
	public function store()
	{
		$data = Input::all();

		$validator = Validator::make($data, Institution::$rules);

		if($validator->fails()){
			return Redirect::route('admin.instituicoes.create')->withErrors($validator)->withInput();
		}

		$institution = Institution::create($data);

		return Redirect::route('admin.instituicoes.index')->withSuccess(Lang::get('crud.create.success', ['element'=>'Instituição']));
	}

	/**
	 * Display the specified resource.
	 * GET /institutions/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /institutions/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$institution = Institution::find($id);

		$checked = $institution->active == '1' ? true : false;

		return View::make('admin.institutions.edit', compact('institution', 'checked'));
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /institutions/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$data = Input::all();

		$validator = Validator::make($data, ['name'=>'required', 'discount'=>'required']);

		if($validator->fails()){
			return Redirect::route('admin.instituicoes.edit', $id)->withErrors($validator)->withInput();
		}

		$institution = Institution::find($id);

		if(!isset($data['active'])){
			$institution->active = 0;
			$institution->save();
		}

		$institution->update($data);

		return Redirect::route('admin.instituicoes.edit', $id)->withSuccess(Lang::get('crud.update.success', ['element'=>'Instituição']));
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /institutions/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$institution = Institution::find($id);

		$institution->delete();

		return Redirect::route('admin.instituicoes.index')->withSuccess(Lang::get('crud.destroy.success', ['element'=>'Instituição']));
	}

}