<?php

class TaxonomiesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /taxonomies
	 *
	 * @return Response
	 */
	protected $layout = 'layouts.base';

	public function index()
	{
		//
		$taxonomies = Taxonomy::orderBy('id','DESC')->paginate(20);

		return View::make('admin.taxonomies.index', compact('taxonomies'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /categories/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		$taxonomies = Taxonomy::orderBy('id','DESC')->lists('name','id');

		return View::make('admin.taxonomies.create', compact('taxonomies'));
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /categories
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		$data = Input::all();

		$validator = Validator::make($data, Taxonomy::$rules);

		if($validator->fails()){
			return Redirect::route('admin.taxonomias.create')->withErrors($validator)->withInput();
		}

		$taxonomy = Taxonomy::create($data);

		$taxonomy->slug = Slugify::slugify($data['name']);
		$taxonomy->save();

		return Redirect::route('admin.taxonomias.index')->withSuccess(Lang::get('crud.create.success', ['element'=>'Taxonomia']));
	}

	/**
	 * Display the specified resource.
	 * GET /categories/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /categories/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$taxonomy = Taxonomy::find($id);

		$taxonomies = Taxonomy::orderBy('id','DESC')->lists('name','id');

		return View::make('admin.taxonomies.edit', compact('taxonomy','taxonomies'));
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /categories/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$taxonomy = Taxonomy::find($id);

		if($taxonomy->name != Input::get('name')){
			$taxonomy->slug = Slugify::slugify(Input::get('name'));
			$taxonomy->save();
		}

		$taxonomy->update(Input::all());



		return Redirect::route('admin.taxonomias.edit',['id'=>$id])->withSuccess(Lang::get('crud.update.success', ['element'=>'Taxonomia']));

	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /categories/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$taxonomy = Taxonomy::find($id);

		$taxonomy->delete();

		return Redirect::route('admin.taxonomias.index')->withSuccess(Lang::get('crud.destroy.success', ['element'=>'Taxonomia']));

	}

}