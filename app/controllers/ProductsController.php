<?php

class ProductsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /products
	 *
	 * @return Response
	 */
	protected $layout = 'layouts.base';
	
	public function results($tax, $cat=null)
	{
		$events = Product::tax($tax)->onlyActive()->nextDate()->orderBy('id','DESC')->get();
		
		$taxonomy = Taxonomy::whereSlug($tax)->first();

		$this->layout->content = View::make('events.index', compact('events', 'taxonomy'));
	}

	public function index()
	{
		//	
		$events =  Product::orderBy('id', 'DESC')->paginate(20);
		return View::make('admin.events.index', compact('events'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /products/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//Create Product
		$states = State::orderBy('name','ASC')->lists('uf','id');
		$facilitators = Facilitator::orderBy('name','ASC')->lists('name','id');
		$categories = Category::orderBy('name','ASC')->lists('name','id');
		$events = Product::orderBy('name','ASC')->lists('name', 'id');
		$titles = Title::orderBy('name')->lists('name','id');
		return View::make('admin.events.create', compact('states', 'titles', 'facilitators', 'categories', 'events'));
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /products
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		$data = Input::all();

		$validator = Validator::make($data, Product::$rules);

		if($validator->fails()){
			return Redirect::route('admin.eventos.create')->withErrors($validator)->withInput();
		}

		$product = Product::create([
			'address' => $data['address'],
			'number' => $data['number'],
			'complement' => $data['complement'],
			'district' => $data['district'],
			'featured' => isset($data['featured']) ? $data['featured'] : null ,
			'name' => $data['name'],
			'duration' => $data['duration'],
			'date_ini' => Helper::ConverterUS($data['date_ini']),
			'date_fin' => Helper::ConverterUS($data['date_fin']),
			'content' => $data['content'],
			'price' => $data['price'],
			'hour' => $data['hour'],
			'facilitator_id' => $data['facilitator_id'],
			'category_id' => $data['category_id'],
			'city_id' => $data['city_id'],
			'info_discount' => $data['info_discount'],
			'related_events' => isset($data['related_events']) ? serialize($data['related_events']) : null,
			'date_event_register_limit' => $data['date_event_register_limit'],
			'active' => isset($data['active']) ? $data['active'] : 0
		]);

		$title = Title::find($data['title_id']);
		$product->title()->associate($title);
		$product->slug = uniqid().'-'.Slugify::slugify($title->name).'-'.Slugify::slugify($data['name']);

		 $allowedExts = array("gif", "jpeg", "jpg", "png");
		    $arr = explode(".", $_FILES["src"]["name"]);
		   
			$extension = end($arr);
			if ((($_FILES["src"]["type"] == "image/gif")
			|| ($_FILES["src"]["type"] == "image/jpeg")
			|| ($_FILES["src"]["type"] == "image/jpg")
			|| ($_FILES["src"]["type"] == "image/png"))
			&& ($_FILES['src']['error'] == '0')
			&& in_array($extension, $allowedExts)){
				
				//Uploading and resizing Images
				$image = $this->upload_file($_FILES['src']);
				$medium = $this->upload_file($_FILES['src'], 541,303,'uploads/eventos/medium',$image->file_dst_name_body);
				$thumb = $this->upload_file($_FILES['src'],307,206,'uploads/eventos/thumb', $image->file_dst_name_body);
		   
		  		$image =  $image->file_dst_name;
		    	$product->src = $image;
			}

		$product->save();

		return Redirect::route('admin.eventos.index')->withSuccess(Lang::get('crud.create.success', ['element'=>'Evento']));
	}

	/**
	 * Display the specified resource.
	 * GET /products/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($slug)
	{
		$event = Product::whereSlug($slug)->first();

		$arr = unserialize($event->related_events);

		$related_events = Product::whereIn('id',$arr)->onlyActive()->nextDate()->get();

		$facilitator_events = Product::whereFacilitator_id($event->facilitator_id)
							->whereNotIn('id',[$event->id])
							->onlyActive()
							->nextDate()
							->get();

		$this->layout->content = View::make('events.show', compact('event','related_events','facilitator_events'));
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /products/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$states = State::orderBy('name','ASC')->lists('uf','id');
		$facilitators = Facilitator::orderBy('name','ASC')->lists('name','id');
		$categories = Category::orderBy('name','ASC')->lists('name','id');
		$events = Product::orderBy('name','ASC')->lists('name', 'id');
		$titles = Title::orderBy('name')->lists('name','id');

		$event = Product::find($id);

		$check_1 = $check_2 = $check_3 = false;

		switch ($event->featured) {
			case '4':
				$check_1 = true;
				break;
			case '2':
				$check_2 = true;
			break;
			case '3':
				$check_3 = true;
			break;
			default:
				# code...
				break;
		}

		$related_events = unserialize($event->related_events);

		$cities = City::whereState_id($event->city->state->id)->orderBy('name')->lists('name','id');

		return View::make('admin.events.edit', compact('event', 'titles', 'states','facilitators', 'categories', 'events','cities','check_1','check_2','check_3','related_events'));
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /products/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$data = Input::all();

		$validator = Validator::make($data, Product::$rules);

		if($validator->fails()){
			return Redirect::route('admin.eventos.edit', $id)->withErrors($validator)->withInput();
		}

		//print_r($data);

		$product = Product::find($id);

		$product->update([
			'address' => $data['address'],
			'number' => $data['number'],
			'complement' => $data['complement'],
			'district' => $data['district'],
			'featured' => isset($data['featured']) ? $data['featured'] : null ,
			'duration' => $data['duration'],
			'date_ini' => Helper::ConverterUS($data['date_ini']),
			'date_fin' => Helper::ConverterUS($data['date_fin']),
			'content' => $data['content'],
			'price' => $data['price'],
			'hour' => $data['hour'],
			'facilitator_id' => $data['facilitator_id'],
			'category_id' => $data['category_id'],
			'city_id' => $data['city_id'],
			'info_discount' => $data['info_discount'],
			'related_events' => isset($data['related_events']) ? serialize($data['related_events']) : null,
			'active' => isset($data['active']) ? $data['active'] : 0,
			'date_event_register_limit' => isset($data['date_event_register_limit']) ? Helper::ConverterUS($data['date_event_register_limit']) : null,
		]);

		$title = Title::find($data['title_id']);
		
		if(($data['name'] != $product->name) || ($title->id != $product->title->id)):
			
			$product->slug = uniqid().'-'.Slugify::slugify($title->name).'-'.Slugify::slugify($data['name']);
		endif;

		$product->name = $data['name'];

        $product->title()->associate($title);


		 $allowedExts = array("gif", "jpeg", "jpg", "png");
		    $arr = explode(".", $_FILES["src"]["name"]);
		   
			$extension = end($arr);
			if ((($_FILES["src"]["type"] == "image/gif")
			|| ($_FILES["src"]["type"] == "image/jpeg")
			|| ($_FILES["src"]["type"] == "image/jpg")
			|| ($_FILES["src"]["type"] == "image/png"))
			&& ($_FILES['src']['error'] == '0')
			&& in_array($extension, $allowedExts)){

				//Deleting existing Image
				if(isset($product->src)):
					File::delete('uploads/eventos/'.$product->src);
					File::delete('uploads/eventos/medium/'.$product->src);
					File::delete('uploads/eventos/thumb/'.$product->src);
				endif;
				
				//Uploading and resizing Images
				$image = $this->upload_file($_FILES['src']);
				$medium = $this->upload_file($_FILES['src'], 541,303,'uploads/eventos/medium',$image->file_dst_name_body);
				$thumb = $this->upload_file($_FILES['src'],307,206,'uploads/eventos/thumb', $image->file_dst_name_body);
		   
		  		$image =  $image->file_dst_name;
		    	$product->src = $image;
		    	
			}

			$product->save();

			return Redirect::route('admin.eventos.edit', $id)->withSuccess(Lang::get('crud.update.success', ['element'=>'Evento']));
	
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /products/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$event = Product::find($id);

		//Deleting existing Image
		if(isset($product->src)):
			File::delete('uploads/eventos/'.$event->src);
			File::delete('uploads/eventos/medium/'.$event->src);
			File::delete('uploads/eventos/thumb/'.$event->src);		
		endif;

		$event->delete();

		return Redirect::route('admin.eventos.index')->withSuccess(Lang::get('crud.destroy.success', ['element'=>'Evento']));
	
	}


}