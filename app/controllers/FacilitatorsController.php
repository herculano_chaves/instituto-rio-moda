<?php

class FacilitatorsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /facilitators
	 *
	 * @return Response
	 */
	protected $layout = 'layouts.base';

	public function index()
	{
		//
		$facilitators = Facilitator::orderBy('id','DESC')->paginate(20);

		return View::make('admin.facilitators.index', compact('facilitators'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /facilitators/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		return View::make('admin.facilitators.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /facilitators
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		$data = Input::all();

		$validator = Validator::make($data, Facilitator::$rules);

		if($validator->fails()){
			return Redirect::route('admin.facilitator.create')->withErrors($validator)->withInput();
		}

		$facilitator = Facilitator::create($data);

	
		$allowedExts = array("gif", "jpeg", "jpg", "png");
		$arr = explode(".", $_FILES["src"]["name"]);
		$extension = end($arr);
			if ((($_FILES["src"]["type"] == "image/gif")
			|| ($_FILES["src"]["type"] == "image/jpeg")
			|| ($_FILES["src"]["type"] == "image/jpg")
			|| ($_FILES["src"]["type"] == "image/png"))
			&& ($_FILES['src']['error'] == '0')
			&& in_array($extension, $allowedExts)){
				
				$image = $this->upload_file($_FILES['src']);
				$facilitator->src =  $image->file_dst_name;
				$facilitator->save();
			}

		
		

		return Redirect::route('admin.facilitator.index')->withSuccess(Lang::get('crud.create.success', ['element'=>'Facilitador']));
	}

	/**
	 * Display the specified resource.
	 * GET /facilitators/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /facilitators/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$facilitator = Facilitator::find($id);

		return View::make('admin.facilitators.edit', compact('facilitator'));
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /facilitators/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
		$facilitator = Facilitator::find($id);

		$facilitator->update(Input::all());

		return Redirect::route('admin.facilitator.edit',['id'=>$id])->withSuccess(Lang::get('crud.update.success', ['element'=>'Facilitador']));

	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /facilitators/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($facilitator)
	{
		//
		$facilitator = Facilitator::find($facilitator);

		if(isset($facilitator->src)):
			unlink(public_path().'/uploads/'.$facilitator->src);
		endif;

		$facilitator->delete();

		return Redirect::route('admin.facilitator.index')->withSuccess(Lang::get('crud.destroy.success', ['element'=>'Facilitador']));
	}

}