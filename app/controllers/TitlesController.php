<?php

class TitlesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /titles
	 *
	 * @return Response
	 */

	public function index()
	{
		$titles = Title::orderBy('name')->paginate(25);
		
		return View::make('admin.titles.index', compact('titles'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /titles/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//		
		return View::make('admin.titles.create', compact('titles'));
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /categories
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		$data = Input::all();

		$validator = Validator::make($data, Title::$rules);

		if($validator->fails()){
			return Redirect::route('admin.titulos.create')->withErrors($validator)->withInput();
		}

		$facilitator = Title::create($data);

		return Redirect::route('admin.titulos.index')->withSuccess(Lang::get('crud.create.success', ['element'=>'Nome de eventos']));
	}

	/**
	 * Display the specified resource.
	 * GET /categories/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /categories/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$title = Title::find($id);

		$titles = Title::orderBy('id','DESC')->lists('name','id');

		return View::make('admin.titles.edit', compact('title','titles'));
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /categories/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$title = Title::find($id);

		$title->update(Input::all());

		return Redirect::route('admin.titulos.edit',['id'=>$id])->withSuccess(Lang::get('crud.update.success', ['element'=>'Nomes de eventos']));

	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /categories/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$title = Title::find($id);

		$title->delete();

		return Redirect::route('admin.titulos.index')->withSuccess(Lang::get('crud.destroy.success', ['element'=>'Nomes de eventos']));

	}

}