<?php

class SubscriptionsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /subscriptions
	 *
	 * @return Response
	 */
	protected $layout = 'layouts.base';

	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /subscriptions/create
	 *
	 * @return Response
	 */
	public function addCart($event_id){

		$event  = Product::find($event_id);
		if(Session::has('cart')){
			
			Session::push('cart', $event_id);
		}else{
			Session::set('cart', [$event_id]);
		}
		
		return Redirect::route('subscription.create')->withSuccess($event->name.' adicionado à sacola de compras!');
	}

	public function deleteItem($event_id){
		
		
		Session::put('cart', array_diff(Session::get('cart'), [$event_id]));
		return Redirect::route('subscription.create');

	}
	public function updateItem($event_id){
		
		
		return Redirect::route('subscription.create');

	}
	public function create()
	{

		$events  = Product::whereIn('id',Session::get('cart'))->get();
		//print_r(Session::get('cart'));
		$this->layout->content = View::make('subscriptions.create', compact('events'));
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /subscriptions
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /subscriptions/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /subscriptions/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /subscriptions/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /subscriptions/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}